#ifndef BC9831BC_7589_448C_A97F_AF1DAED602F6
#define BC9831BC_7589_448C_A97F_AF1DAED602F6

#include <stdint.h>

typedef struct aes128 {
    uint8_t state[16];
    uint8_t expandedKeys[176];
} aes128_t;

void aes128_init(aes128_t* this, const uint8_t* key);
int aes128_encrypt(aes128_t* this, uint8_t* out, const uint8_t* in, int count);
int aes128_decrypt(aes128_t* this, uint8_t* out, const uint8_t* in, int count);
int aes128_ctr(aes128_t* this, uint64_t nonce, uint8_t* out, const uint8_t* in, int count);

#endif /* BC9831BC_7589_448C_A97F_AF1DAED602F6 */
