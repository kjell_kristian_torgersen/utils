#ifndef A7C537DA_8CC5_466A_AEB7_F91407F9F71B
#define A7C537DA_8CC5_466A_AEB7_F91407F9F71B

typedef struct iir {
    int order;
    float strenght;
    float* y;
} iir_t;

/** \brief Initialize the IIR filter. It is actually 1. order IIR low pass filters in cascade. The order is the number of stages. The strenght is the amount of filtering.
 * \param *this The IIR filter to initialize.
 * \param order The number of stages.
 * \param strenght The amount of filtering.
 * \param *storage The storage to use. Must be at least order * floats long.
 * \sa iir_filter
 */
void iir_init(iir_t* this, int order, float strenght, float* storage);

/** \brief Filter a single sample.
 * \param *this The IIR filter to filter with.
 * \param x The sample to filter.
 * \return The filtered sample.
 * \see iir_init
 */
float iir_filter(iir_t* this, float x);

#endif /* A7C537DA_8CC5_466A_AEB7_F91407F9F71B */
