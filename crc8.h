#ifndef A2235F82_F223_462E_8B4D_C7A94935DA41
#define A2235F82_F223_462E_8B4D_C7A94935DA41

#include <stdbool.h>
#include <stdint.h>

uint8_t crc8_reverse(uint8_t b);
uint8_t crc8_update(uint8_t crc, uint8_t poly, uint8_t a);
void crc8_fill_table(uint8_t* table, uint8_t poly, bool refin, bool refout);
uint8_t crc8_use_table(const uint8_t* table, uint8_t crc, const void* buf, int count);
uint8_t crc8_process(uint8_t poly, uint8_t crc, bool refin, const void* buf, int count);

#endif /* A2235F82_F223_462E_8B4D_C7A94935DA41 */
