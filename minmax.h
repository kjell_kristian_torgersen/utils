#ifndef AD379635_9D35_43D8_B57D_EBCBA06537FF
#define AD379635_9D35_43D8_B57D_EBCBA06537FF

#include <stdint.h>

/// \brief Keep track of min, max and the sum of values
typedef struct minmax {
    uint16_t min;
    uint16_t max;
    uint32_t sum;
} minmax_t;

void minmax_init(minmax_t* mm);
void minmax_processValue(minmax_t* mm, uint16_t val);
void minmax_processValues(minmax_t* mm, const uint16_t* val, int count);

#endif /* AD379635_9D35_43D8_B57D_EBCBA06537FF */
