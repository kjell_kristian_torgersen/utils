#ifndef C77110AE_9D12_40A2_B069_CEF8ED68107A
#define C77110AE_9D12_40A2_B069_CEF8ED68107A

#include <stdint.h>

typedef int (*cobs_write_t)(void* arg, const void* buf, int count);

typedef struct cobs_encoder {
    uint8_t buffer[255];
    uint8_t counter;
    cobs_write_t write;
    void* arg;
} cobs_encoder_t;

/** \brief Init a new cobs encoder.
 * Use one or multiple calls cobs_encoder_write() to write to the encoder.
 * Then use cobs_encoder_demarcate() to mark the end of the data. Start a new message by performing more cobs_encoder_write() calls.
 * The function specified in the write callback will be called to write the data to the output automatically when required.
 * \param this The encoder to init.
 * \param write The function to call to write data.
 * \return 0 on success, -1 on failure.
 * \sa cobs_encoder_write
 * \sa cobs_encoder_demarcate
 */
void cobs_encoder_init(cobs_encoder_t* encoder, cobs_write_t write, void* arg);
/** \brief Write data to the encoder.
 * \param this The encoder to write to.
 * \param buf Data to write.
 * \param count Number of bytes to write.
 * \return The number of bytes written.
 */
int cobs_encoder_write(cobs_encoder_t* encoder, const void* buf, int count);
/** \brief Demarcate the message.
 * \param this The encoder to demarcate.
 * */
void cobs_encoder_demarcate(cobs_encoder_t* encoder);

#endif /* C77110AE_9D12_40A2_B069_CEF8ED68107A */
