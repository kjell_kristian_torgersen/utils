#ifndef A1C73F97_5F64_4141_9038_1914198CAB5B
#define A1C73F97_5F64_4141_9038_1914198CAB5B

typedef void (*datareceived_callback_t)(void* userdata, const void* data, int size);

typedef struct datareceived_event {
    datareceived_callback_t callback; ///< Function to be called when data is received
    void* userdata; ///< Userdata to be passed to the callback
    struct datareceived_event* next; ///< Next event in the list
} datareceived_event_t;

/** \brief Add a subscriber to the datareceived event.
 * \param head Pointer to the head of the list. Initially this could just be a NULL pointer, which is updated to point to the new subscriber.
 * \param event An instance of the datareceived_event_t should be allocated somewhere permanently.
 * \param callback The callback to be called when data is received.
 * \param userdata The userdata to be passed to the callback.
 */
void datareceived_event_subscribe(datareceived_event_t** head, datareceived_event_t* event, datareceived_callback_t callback, void* userdata);

/** \brief Publish data to the subscribers of the datareceived event.
 * \param head Pointer to the head of the list.
 * \param data The data to be published.
 * \param size The size of the data.
 */
void datareceived_event_publish(datareceived_event_t* head, const void* data, int size);

#endif /* A1C73F97_5F64_4141_9038_1914198CAB5B */
