#ifndef C3B461A7_2228_44A2_BF40_437DAD162ED6
#define C3B461A7_2228_44A2_BF40_437DAD162ED6

#include <stdint.h>

void random_init(uint32_t seed);
void random_get(void* data, uint32_t size);
uint8_t random_u8(void);
uint16_t random_u16(void);
uint32_t random_u32(void);
uint64_t random_u64(void);

#endif /* C3B461A7_2228_44A2_BF40_437DAD162ED6 */
