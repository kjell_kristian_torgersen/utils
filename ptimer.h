#ifndef A6E37B2D_F0AF_4D8A_8445_0D71BAE929CE
#define A6E37B2D_F0AF_4D8A_8445_0D71BAE929CE
/** \file Periodic timer implementation. */
#include <stdint.h>

typedef struct ptimer {
    uint32_t start; ///< Start time of the timer.
    uint32_t period; ///< Period of the timer.
    void (*callback)(void* arg); ///< Callback function to be called when the timer expires.
    void* arg; ///< Argument to be passed to the callback function.
    struct ptimer* next; ///< Next timer in the list.
} ptimer_t;

/** \brief Configure a timer. This function can be called multiple times with the same ptimer_t * to update period, callback and/or arg.
 * \param this The timer to configure.
 * \param period The period of the timer in ticks.
 * \param callback The callback function to call when the timer fires.
 * \param arg The argument to pass to the callback function.
 * \return void
 * \sa ptimer_update
 * \sa ptimer_t
 */
void ptimer_configure(ptimer_t* this, uint32_t period, void (*callback)(void* arg), void* arg);

/** \brief Call this function regularly, preferably at least as often at ticks changes.
 * \param ticks The current tick count.
 */
void ptimer_update(uint32_t ticks);

#endif /* A6E37B2D_F0AF_4D8A_8445_0D71BAE929CE */
