#include <stdint.h>
#include <string.h>

#include "../chacha20.h"

#include "../misc.h"

#define U32V(v) ((uint32_t)(v) & ((uint32_t)0xFFFFFFFF))
#define ROTL32(v, n) (U32V((v) << (n)) | ((v) >> (32 - (n))))

#define U8V(v) ((uint8_t)(v))

#define U8TO32(p) \
    (((uint32_t)((p)[0]) << 0) | ((uint32_t)((p)[1]) << 8) | ((uint32_t)((p)[2]) << 16) | ((uint32_t)((p)[3]) << 24))

#define U32TO8(p, v)             \
    do {                         \
        (p)[0] = U8V((v));       \
        (p)[1] = U8V((v) >> 8);  \
        (p)[2] = U8V((v) >> 16); \
        (p)[3] = U8V((v) >> 24); \
    } while (0)

#define ROTATE(v, c) (ROTL32(v, c))
#define XOR(v, w) ((v) ^ (w))
#define PLUS(v, w) (U32V((v) + (w)))
#define PLUSONE(v) (PLUS((v), 1))

#define QR(a, b, c, d)                  \
    x[a] = PLUS(x[a], x[b]);            \
    x[d] = ROTATE(XOR(x[d], x[a]), 16); \
    x[c] = PLUS(x[c], x[d]);            \
    x[b] = ROTATE(XOR(x[b], x[c]), 12); \
    x[a] = PLUS(x[a], x[b]);            \
    x[d] = ROTATE(XOR(x[d], x[a]), 8);  \
    x[c] = PLUS(x[c], x[d]);            \
    x[b] = ROTATE(XOR(x[b], x[c]), 7);

static void generate_block(uint8_t* output, const uint32_t* input)
{
    uint32_t x[16];
    for (int i = 0; i < 16; i++)
        x[i] = input[i];

    for (int i = 0; i < 10; i++) {
        QR(0, 4, 8, 12);
        QR(1, 5, 9, 13);
        QR(2, 6, 10, 14);
        QR(3, 7, 11, 15);
        QR(0, 5, 10, 15);
        QR(1, 6, 11, 12);
        QR(2, 7, 8, 13);
        QR(3, 4, 9, 14);
    }

    for (int i = 0; i < 16; i++) {
        x[i] = PLUS(x[i], input[i]);
    }
    for (int i = 0; i < 16; i++) {
        U32TO8(output + i * 4, x[i]);
    }
}

void chacha20_keysetup(chacha20_t* this, const uint8_t* k)
{
    const char* constants = "expand 32-byte k";
    this->x[0] = U8TO32(constants + 0);
    this->x[1] = U8TO32(constants + 4);
    this->x[2] = U8TO32(constants + 8);
    this->x[3] = U8TO32(constants + 12);
    this->x[4] = U8TO32(k + 0);
    this->x[5] = U8TO32(k + 4);
    this->x[6] = U8TO32(k + 8);
    this->x[7] = U8TO32(k + 12);
    this->x[8] = U8TO32(k + 16);
    this->x[9] = U8TO32(k + 20);
    this->x[10] = U8TO32(k + 24);
    this->x[11] = U8TO32(k + 28);
}

void chacha20_set_nonce(chacha20_t* this, uint64_t nonce, uint64_t counter)
{
    this->x[12] = counter;
    this->x[13] = counter >> 32;
    this->x[14] = nonce;
    this->x[15] = nonce >> 32;
    this->output_idx = 64;
}

void chacha20_init(chacha20_t* this, const uint8_t* key)
{
    chacha20_keysetup(this, key);
}

#if 0
int chacha20_process(chacha20_t * this, uint8_t * dest, const uint8_t * src, int count) 
{
	//uint32_t x[16];
	//uint8_t output[64];
	//uint8_t output_idx;
	//memcpy(x, this->x, sizeof(x));
	//memcpy(output, this->output, 64);
	//output_idx = this->output_idx;
	int i = 0;
	
	// If the output used up, generate a new block.
	if(this->output_idx == 64) {
		this->output_idx = 0;
		generate_block(this->output, this->x);
		this->x[12] = PLUSONE(this->x[12]);
		if(!this->x[12]) {
			this->x[13] = PLUSONE(this->x[13]);
		}	
	}

	for(;;) {
		int n = MIN(64 - this->output_idx, count - i);
		for(int j = 0; j < n; j++) {
			dest[j] = src[j] ^ this->output[this->output_idx++];
		}
		i += n;
		if(i >= count) break;
		dest += n;
		src += n;
		this->output_idx = 0;
		generate_block(this->output, this->x);
		this->x[12] = PLUSONE(this->x[12]);
		if(!this->x[12]) {
			this->x[13] = PLUSONE(this->x[13]);
		}
	}
	//memcpy(this->x, x, sizeof(x));
	//memcpy(this->output, output, 64);
	//this->output_idx = output_idx;
	return count;
}
#endif

int chacha20_process(chacha20_t* this, uint8_t* dest, const uint8_t* src, int count)
{
    uint8_t output[64];
    int remainingBytes = count;
    if (count < 1)
        return 0;
    int i = 0;

    // if there is a partial block from last call, process it first
    if (this->output_idx != 64) {
        int n = MIN(64 - this->output_idx, count);
        for (int j = 0; j < n; j++) {
            dest[j] = src[j] ^ this->output[this->output_idx++];
        }
        i += n;
        if (i >= count)
            return count;
        dest += n;
        src += n;
        remainingBytes -= n;
    }

    // then process as many whole blocks as required, and then any remaining bytes
    for (;;) {
        generate_block(output, this->x);
        this->x[12] = PLUSONE(this->x[12]);
        if (!this->x[12]) {
            this->x[13] = PLUSONE(this->x[13]);
        }
        if (remainingBytes <= 64) {
            for (i = 0; i < remainingBytes; i++) {
                dest[i] = src[i] ^ output[i];
            }
            this->output_idx = remainingBytes;
            memcpy(this->output, output, 64);
            return count;
        }
        for (i = 0; i < 64; i++) {
            dest[i] = src[i] ^ output[i];
        }
        remainingBytes -= 64;
        dest += 64;
        src += 64;
    }
}
