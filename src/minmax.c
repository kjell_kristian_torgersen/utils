#include "../minmax.h"

void minmax_init(minmax_t* mm)
{
    mm->min = UINT16_MAX;
    mm->max = 0;
    mm->sum = 0;
}
void minmax_processValue(minmax_t* mm, uint16_t val)
{
    if (val < mm->min) {
        mm->min = val;
    }
    if (val > mm->max) {
        mm->max = val;
    }
    mm->sum += val;
}

void minmax_processValues(minmax_t* mm, const uint16_t* val, int count)
{
    for (int i = 0; i < count; i++) {
        if (val[i] < mm->min) {
            mm->min = val[i];
        }
        if (val[i] > mm->max) {
            mm->max = val[i];
        }
        mm->sum += val[i];
    }
}