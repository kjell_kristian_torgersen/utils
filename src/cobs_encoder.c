#include <stddef.h>

#include "../cobs_encoder.h"

void cobs_encoder_init(cobs_encoder_t *this, cobs_write_t write, void *arg)
{
	this->counter = 1;
	this->write   = write;
	this->arg     = arg;
}

int cobs_encoder_write(cobs_encoder_t *this, const void *buf, int count)
{
	if (this->write == NULL) {
		return count;
	}
	const uint8_t *data = buf;
	for (int i = 0; i < count; i++) {
		if (data[i] != '\0') {
			this->buffer[this->counter] = data[i];
			this->counter++;
		}

		if (data[i] == 0 || this->counter == 255) {
			this->buffer[0] = this->counter;
			this->write(this->arg, this->buffer, this->counter);
			this->counter = 1;
		}
	}
	return count;
}

void cobs_encoder_demarcate(cobs_encoder_t *this)
{
	if (this->write == NULL) {
		return;
	}
	uint8_t tmp	= '\0';
	this->buffer[0] = this->counter;
	this->write(this->arg, this->buffer, this->counter);
	this->write(this->arg, &tmp, 1);
	this->counter = 1;
}