#include <math.h>
#include <stdint.h>
#include <stdio.h>

#include "../misc.h"

static void printHexLine(const uint8_t* arr, int count)
{
    for (int i = 0; i < 16; i++) {
        if (i < count) {
            printf("%02X ", arr[i]);
        } else {
            printf("   ");
        }
        if ((i & 0x3) == 0x3)
            printf(" ");
        if ((i & 0x7) == 0x7)
            printf("  ");
    }
    for (int i = 0; i < 16; i++) {

        if (i < count) {
            if ((arr[i] >= 0x20) && (arr[i] < 0x80)) {
                printf("%c", arr[i]);
            } else {
                printf(".");
            }
        } else {
            printf(" ");
        }
        if ((i & 0x7) == 0x7)
            printf("  ");
    }
    printf("\n");
}

/** \brief Display an array in hex, hex-editor style for debugging. */
void misc_showArr(const void* buf, int count)
{
    const uint8_t* arr = buf;
    // work out how many 16 byte lines and remaining bytes
    int lines = count / 16;
    int rest = count % 16;

    // print all full lines
    for (int i = 0; i < lines; i++) {
        printf("%04x:   ", 16 * i);
        printHexLine(&arr[16 * i], 16);
    }

    // if partial line, write partial line
    if (rest) {
        printf("%04x:   ", lines * 16);
        printHexLine(&arr[16 * lines], rest);
    }

    printf("\n");
}

/** \brief float support doesn't seem to be enabled for printf. Also enabling floating point support in printf generates a lot of machine code.
 * Usually it is sufficient just to print the numbers with 1-3 digits after decimal separator.
 */
void misc_print_float(float f) { printf("%03i.%03u ", (int)f, ((int)(fabsf(f) * 1000)) % 1000); }
