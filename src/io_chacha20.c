#include <stdarg.h>
#include <stddef.h>

#include "config.h"

#if IO_CHACHA20_NUM > 0
#include "api/io.h"
#include "utils/random.h"

#include "../chacha20.h"

static struct io {
    const io_vtable_t* vtable;
    io_t* next;
    chacha20_t* chacha20;
} io_chacha20[IO_CHACHA20_NUM] = { 0 };

static int ioread(io_t* this, void* buf, int len)
{
    uint8_t src[len + 8];
    int n = io_read(this->next, src, len);
    if (n < 9) {
        return 0;
    }
    chacha20_set_nonce(this->chacha20, *(uint64_t*)src, 0);
    return chacha20_process(this->chacha20, buf, &src[8], n - 8);
}

static int iowrite(io_t* this, const void* buf, int len)
{
    uint8_t cipher_text[len];
    chacha20_process(this->chacha20, cipher_text, buf, len);
    return io_write(this->next, cipher_text, len);
}

static int ioseek(io_t* this, int offset, io_whence_t whence)
{
    return io_seek(this->next, offset, whence);
}

static int ioctl(io_t* this, int request, va_list arg)
{
    return io_ctl(this->next, request, arg);
}

static void ioflush(io_t* this)
{
    io_flush(this->next);
    uint64_t nonce = random_u64();
    chacha20_set_nonce(this->chacha20, nonce, 0);
    io_write(this->next, &nonce, sizeof(nonce));
}

static void ioclose(io_t* this)
{
    io_close(this->next);
    this->vtable = NULL;
}

static const io_vtable_t io_vtable = {
    .read = ioread,
    .write = iowrite,
    .seek = ioseek,
    .ctl = ioctl,
    .flush = ioflush,
    .close = ioclose,
};

static io_t* find_free(void)
{
    for (int i = 0; i < IO_CHACHA20_NUM; i++) {
        if (!io_chacha20[i].vtable) {
            return &io_chacha20[i];
        }
    }
    return NULL;
}

io_t* io_chacha20_init(io_t* next, chacha20_t* chacha20)
{
    io_t* this = find_free();
    if (!this)
        return NULL;
    this->vtable = &io_vtable;
    this->next = next;
    this->chacha20 = chacha20;
    uint64_t nonce = random_u64();
    chacha20_set_nonce(this->chacha20, nonce, 0);
    io_write(this->next, &nonce, sizeof(nonce));
    return this;
}
#endif