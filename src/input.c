#include "../input.h"

bool input_parseChar(linbuf_t* lb, char c)
{
    bool ret = false;
    if (c == '\b') {
        linbuf_erase(lb, 1);
    } else if (c == '\r' || c == '\n') {
        char tmp = '\0';
        linbuf_write(lb, &tmp, 1);
        ret = true;
    } else {
        linbuf_write(lb, &c, 1);
    }
    return ret;
}

const char* input_str(linbuf_t* lb)
{
    return linbuf_get(lb);
}

void input_clear(linbuf_t* lb)
{
    linbuf_clear(lb);
}