#include <stddef.h>

#include "../datareceived_event.h"

void datareceived_event_subscribe(datareceived_event_t** head, datareceived_event_t* event, datareceived_callback_t callback, void* userdata)
{
    // set or update callback and userdata
    event->callback = callback;
    event->userdata = userdata;

    // check if event already is added to list, in that case don't add it to the list?
    for (datareceived_event_t* e = *head; e != NULL; e = e->next) {
        if (event == e) {
            return;
        }
    }

    // add event to list. Let this event->next point all existing events and set this event as head of the linked list
    event->next = *head;
    *head = event;
}

void datareceived_event_publish(datareceived_event_t* head, const void* data, int size)
{
    for (datareceived_event_t* event = head; event != NULL; event = event->next) {
        if (event->callback) {
            event->callback(event->userdata, data, size);
        }
    }
}