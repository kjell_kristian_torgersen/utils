#include <string.h>

#include "../klv.h"

#include "api/flash.h"
#include "utils/crc8.h"

#define ENTRY_EMPTY 0xFFFFFFFF
#define ENTRY_SIZE 3

#define POLY 0x07

#ifndef KLV_PADDING
#define KLV_PADDING 2
#endif

static void klv_write_flash(klv_t* klv, const void* data, int count)
{
    flash_write(klv->flash, klv->write_idx, data, count);
    klv->write_idx += count;
}

static uint32_t get_block_usage(klv_t* klv, int block)
{
    uint32_t start;
    if (block == 0) {
        start = klv->addr_a;
    } else {
        start = klv->addr_b;
    }

    for (int i = klv->block_size - 1; i != 0; i--) {
        uint8_t b;
        flash_read(klv->flash, start + i, &b, 1);
        if (b != 0xFF) {
            return i;
        }
    }

    return 0;
}
static void block_copy(klv_t* klv, int from, int count)
{
    uint8_t buf[count];

    flash_read(klv->flash, from, buf, count);
    flash_write(klv->flash, klv->write_idx, buf, count);
    klv->write_idx += count;
}

extern void display_flash(flash_t* flash);

static void move_to_new_block(int current_block, klv_t* klv)
{
    if (current_block) {
        klv->write_idx = klv->addr_a;
    } else {
        klv->write_idx = klv->addr_b;
    }
    for (int i = 0; i < 256; i++) {
        if (klv->entries[i] != ENTRY_EMPTY) {
            uint8_t entry_len;
            flash_read(klv->flash, klv->entries[i] + 1, &entry_len, sizeof(entry_len));
            // display_flash(klv->flash);
            uint32_t new_place = klv->write_idx;
            int len_without_padding = ENTRY_SIZE + entry_len;
            int padding_required = (len_without_padding) & (KLV_PADDING - 1);
            int len_with_padding = padding_required ? (len_without_padding + (KLV_PADDING - padding_required)) : (len_without_padding);
            block_copy(klv, klv->entries[i], len_with_padding);
            klv->entries[i] = new_place;
            // display_flash(klv->flash);
        }
    }

    if (current_block) {
        flash_erase(klv->flash, klv->addr_b, klv->block_size);
    } else {
        flash_erase(klv->flash, klv->addr_a, klv->block_size);
    }
}

void scan_block(klv_t* klv, int block)
{
    uint32_t start;
    int read_pos;
    if (block == 0) {
        start = read_pos = klv->addr_a;
    } else {
        start = read_pos = klv->addr_b;
    }

    // read all entries from flash
    while ((read_pos - start) < (klv->block_size)) {
        uint8_t key;
        uint8_t len;

        flash_read(klv->flash, read_pos, &key, sizeof(uint8_t));
        flash_read(klv->flash, read_pos + 1, &len, sizeof(uint8_t));

        int len_without_padding = len + ENTRY_SIZE;
        int padding_required = len_without_padding & (KLV_PADDING - 1);
        int total_len = padding_required ? len_without_padding + (KLV_PADDING - padding_required) : len_without_padding;

        uint8_t buf[total_len];
        buf[0] = key;
        buf[1] = len;

        flash_read(klv->flash, read_pos + 2, buf + 2, total_len - 2);
        uint8_t crc = crc8_process(POLY, 0, false, buf, total_len);

        // if reading empty flash, then it will appear as a block of length 255 containing only 0xFF, but with wrong checksum.
        if (key == 0xFF && len == 0xFF) {
            break;
        }
        if (crc == 0) {
            // valid entry found
            klv->entries[key] = read_pos; // store position of entry
            read_pos += total_len; // jump to next entry
        } else {
            // corrupt entry found
            read_pos++;
        }
    }
    klv->write_idx = read_pos;
}

void klv_init(klv_t* klv, flash_t* flash, uint32_t addr_a, uint32_t addr_b, uint32_t block_size)
{
    klv->flash = flash;
    klv->addr_a = addr_a;
    klv->addr_b = addr_b;
    klv->block_size = block_size;
    memset(klv->entries, 0xFF, sizeof(klv->entries));

    uint32_t a;
    uint32_t b;

    flash_read(flash, addr_a, &a, sizeof(uint32_t));
    flash_read(flash, addr_b, &b, sizeof(uint32_t));

    if ((a == ENTRY_EMPTY) && (b == ENTRY_EMPTY)) {
        klv->write_idx = addr_a;
        // Both empty. No need to scan
    } else if ((a != ENTRY_EMPTY) && (b != ENTRY_EMPTY)) {
        // TODO: handle if both block are not empty. That means that the flash has failed to erase properly.
        // Probably a reset while copying from the full block to the initially empty.
        // The strategy then is to erase the smallest block and re-do the move operation with the move_to_new_block() function
        // Scan both blocks backwards until we find the first non 0xFF entry. Then see which one that is smaller.
        uint32_t a_size = get_block_usage(klv, 0);
        uint32_t b_size = get_block_usage(klv, 1);
        if (a_size > b_size) {
            // erase block b, and move data to block b
            flash_erase(flash, addr_b, klv->block_size);
            // scan block A to know what to move
            scan_block(klv, 0);
            move_to_new_block(0, klv);

        } else {
            // erase block a, and move data to block a
            flash_erase(flash, addr_a, klv->block_size);
            scan_block(klv, 1);
            // scan block B to know what to move
            move_to_new_block(1, klv);
        }
    } else {
        // one block is empty, the other is not. Scan the non-empty block.
        if (a != ENTRY_EMPTY) {
            scan_block(klv, 0);
        } else {
            scan_block(klv, 1);
        }
    }
}

static int get_current_block(klv_t* klv, uint32_t pos)
{
    if ((pos >= klv->addr_a) && (pos < (klv->addr_a + klv->block_size))) {
        // in the first block
        return 0;
    } else {
        // in the second block
        return 1;
    }
}

/**
 * @brief Write an entry into the KLV structure in flash. If the data already is in the flash, then it does nothing.
 * @param klv The klv object
 * @param key The key of the entry
 * @param data The data of the entry
 * @param len The length of the data
 * @return The size of the current block
 */
ret_t klv_write(klv_t* klv, uint8_t key, const void* data, uint8_t len)
{
    int len_without_padding = len + ENTRY_SIZE;
    int padding_required = len_without_padding & (KLV_PADDING - 1);
    int len_with_padding = padding_required ? len_without_padding + (KLV_PADDING - padding_required) : len_without_padding;

    if ((klv->entries[key] != ENTRY_EMPTY) && (len != 0)) {
        // entry already exists
        uint8_t buf[len];
        uint8_t n = klv_read(klv, key, buf, len);
        if ((n != 0) && !memcmp(buf, data, len)) {
            // data is the same, no need to write
            return len;
        }
    }

    int current_block = get_current_block(klv, (klv->write_idx - 1));
    if (klv->write_idx + len_with_padding - (current_block ? klv->addr_b : klv->addr_a) > klv->block_size) { // not enough space in block
        // display_flash(klv->flash);
        move_to_new_block(current_block, klv);
        // display_flash(klv->flash);
    }
    klv->entries[key] = klv->write_idx; // flash_seek(klv->flash, 0, SEEK_CUR);

    uint8_t crc = 0x0;

    uint8_t block[len_with_padding];
    memset(block, 0, sizeof(block));

    block[0] = key;
    block[1] = len;
    memcpy(block + 2, data, len);

    crc = crc8_process(POLY, crc, false, block, sizeof(block) - 1);
    block[sizeof(block) - 1] = crc;

    klv_write_flash(klv, block, sizeof(block));

    return len;
}

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))
uint8_t klv_read(klv_t* klv, uint8_t key, void* data, uint8_t len)
{
    uint8_t ret;
    if (klv->entries[key] != ENTRY_EMPTY) { // entry found
        uint8_t n = 0;
        flash_read(klv->flash, klv->entries[key] + 1, &n, 1);
        ret = MIN(n, len);
        flash_read(klv->flash, klv->entries[key] + 2, data, ret);
    } else { // entry not found
        ret = 0;
    }
    return ret;
}

ret_t klv_erase(klv_t* klv, uint8_t key)
{
    if (klv->entries[key] != ENTRY_EMPTY) { // entry already exists
        // since we aren't marking the key as empty, we just re-write the entry with the empty value
        klv_write(klv, key, NULL, 0);
    }
    return RET_OK;
}
