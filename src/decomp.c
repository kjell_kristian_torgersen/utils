#include <stdio.h>

#include "../decomp.h"
#include "comp_priv.h"

static uint8_t read_nibble(decomp_t* decomp)
{
    if (decomp->idx == 2) {
        decomp->reader(decomp->reader_arg, &decomp->byte, 1);
        decomp->idx = 0;
    }
    if (decomp->idx == 0) {
        decomp->idx++;
        return decomp->byte >> 4;
    } else {
        decomp->idx++;
        return decomp->byte & 0x0F;
    }
}
void decomp_init(decomp_t* decomp, decomp_read_t reader, void* arg)
{
    decomp->reader = reader;
    decomp->reader_arg = arg;
    decomp->value = 0;
    decomp->last_value = 0;
    decomp->repeat = 0;
    decomp->byte = 0;
    decomp->idx = 2;
    /*for (int i = 0; i < 6; i++) {
        printf("%i ", read_nibble(decomp));
    }
    printf("\n");
    fflush(stdout);*/
}

bool decomp_read(decomp_t* decomp, uint16_t* value)
{
    if (decomp->repeat == 63) {
        return false;
    }
    if (decomp->repeat) {
        decomp->repeat--;
        decomp->last_value = decomp->value;
        *value = decomp->value;
        return true;
    }
    uint8_t nibble = read_nibble(decomp);
    if ((nibble & COMP_REPEAT_MASK) == COMP_REPEAT) {
        uint8_t repeat = (nibble & ~COMP_REPEAT_MASK) << 4;
        repeat |= read_nibble(decomp);
        decomp->repeat = repeat;

        return decomp_read(decomp, value);
    } else if ((nibble & COMP_DIFF_MASK) == COMP_DIFF) {
        int diff = (nibble & ~COMP_DIFF_MASK) - 4;
        decomp->last_value = decomp->value;
        decomp->value += diff;
        *value = decomp->value;
        return true;
    } else if ((nibble & COMP_SET_MASK) == COMP_SET) {
        decomp->last_value = decomp->value;
        decomp->value = (nibble & ~COMP_SET_MASK) << 12;
        decomp->value |= read_nibble(decomp) << 8;
        decomp->value |= read_nibble(decomp) << 4;
        decomp->value |= read_nibble(decomp);
        *value = decomp->value;
        return true;
    }
    return false;
}