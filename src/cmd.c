#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "../cmd.h"

uint64_t cmd_uint64(const char* __restrict__ __n, char** __restrict__ __end_PTR)
{
    return strtoull(__n, __end_PTR, 0);
}

static int cmdcmp(const char* s1, const char* s2, int n)
{
    if (!strncmp(s1, s2, n)) {
        if ((s1[n] == '\0' || s1[n] == ' ') && (s2[n] == '\0' || s2[n] == ' ')) {
            return 0;
        }
    }
    return 1;
}

cmd_response_t cmd_parse(const char* cmd, cmd_ctx_t* ctx, const cmd_t* cmds, int ncmds)
{
    for (int i = 0; i < ncmds; i++) {
        int n = strlen(cmds[i].cmd);
        if (cmdcmp(cmd, cmds[i].cmd, n) == 0) {
            if (cmd[n] == '\0') {
                return cmds[i].callback(ctx, NULL);
            } else {
                return cmds[i].callback(ctx, &cmd[n + 1]);
            }
        }
    }
    return CMD_RESPONSE_INVALID_COMMAND;
}

const char* cmd_response_str(cmd_response_t response)
{
    switch (response) {
    case CMD_RESPONSE_OK:
        return "OK";
    case CMD_RESPONSE_ERROR:
        return "ERROR";
    case CMD_RESPONSE_INVALID_ARGUMENT:
        return "INVALID ARGUMENT";
    case CMD_RESPONSE_INVALID_COMMAND:
        return "INVALID COMMAND";
    case CMD_RESPONSE_INVALID_STATE:
        return "INVALID STATE";
    case CMD_RESPONSE_INVALID_VALUE:
        return "INVALID VALUE";
    case CMD_RESPONSE_NOT_IMPLEMENTED:
        return "NOT IMPLEMENTED";
    case CMD_RESPONSE_NOT_SUPPORTED:
        return "NOT SUPPORTED";
    case CMD_RESPONSE_UNKNOWN:
        return "UNKNOWN";
    }
    return "UNKNOWN";
}