#include <stdarg.h>
#include <stddef.h>

#include "../cobs.h"
#include "../cobs_encoder.h"
#include "api/io.h"

#include "config.h"

#if IO_COBS_NUM > 0

static struct io {
    const io_vtable_t* vtable;
    io_t* next;
    cobs_encoder_t encoder;
    uint8_t* rxbuf;
    int rxbufsize;
    int rxbufidx;
} io_cobs[IO_COBS_NUM] = { 0 };

static int ioread(io_t* this, void* buf, int len)
{
    uint8_t b = 0;

    while (io_read(this->next, &b, 1) == 1) {
        if (b == 0) {
            int n = cobs_decode(buf, len, this->rxbuf, this->rxbufidx);
            this->rxbufidx = 0;
            return n;
        } else {
            if (this->rxbufidx < this->rxbufsize) {
                this->rxbuf[this->rxbufidx++] = b;
            }
        }
    }
}

static int iowrite(io_t* this, const void* buf, int len)
{
    return cobs_encoder_write(&this->encoder, buf, len);
}

static int ioseek(io_t* this, int offset, io_whence_t whence)
{
    return io_seek(this->next, offset, whence);
}

static int ioctl(io_t* this, int request, va_list arg)
{
    return io_ctl(this->next, request, arg);
}

static void ioflush(io_t* this)
{
    io_flush(this->next);
    cobs_encoder_demarcate(&this->encoder);
}

static void ioclose(io_t* this)
{
    io_close(this->next);
    this->vtable = NULL;
}

static const io_vtable_t io_vtable = {
    .read = ioread,
    .write = iowrite,
    .seek = ioseek,
    .ctl = ioctl,
    .flush = ioflush,
    .close = ioclose,
};

static io_t* find_free(void)
{
    for (int i = 0; i < IO_COBS_NUM; i++) {
        if (!io_cobs[i].vtable) {
            return &io_cobs[i];
        }
    }
    return NULL;
}

static int write(void* arg, const void* buf, int len)
{
    io_t* this = arg;
    return io_write(this->next, buf, len);
}

io_t* io_cobs_init(io_t* next, void* rxbuf, int rxbufsize)
{
    io_t* this = find_free();
    if (!this)
        return NULL;
    cobs_encoder_init(&this->encoder, write, this);
    this->vtable = &io_vtable;
    this->next = next;
    this->rxbuf = rxbuf;
    this->rxbufsize = rxbufsize;
    return this;
}
#endif