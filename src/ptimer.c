#include <stddef.h>

#include "../ptimer.h"

static ptimer_t* head = NULL;

void ptimer_configure(ptimer_t* this, uint32_t period, void (*callback)(void* arg), void* arg)
{

    this->period = period;
    this->start = 0;
    this->callback = callback;
    this->arg = arg;

    // Check if timer already is in list
    for (ptimer_t* it = head; it != NULL; it = it->next) {
        if (this == it) {
            return;
        }
    }

    // Add timer to list
    this->next = head;
    head = this;
}

void ptimer_update(uint32_t ticks)
{
    for (ptimer_t* this = head; this != NULL; this = this->next) {
        if ((this->period != 0) && (ticks - this->start >= this->period)) {
            this->start = ticks;
            if (this->callback) {
                this->callback(this->arg);
            }
        }
    }
}