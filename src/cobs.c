#include <stddef.h>
#include <stdint.h>

int cobs_encode(uint8_t* dest, int destsize, const uint8_t* src, int srcsize)
{
    uint8_t* encode = dest; // Encoded byte pointer
    uint8_t* codep = encode++; // Output code pointer
    uint8_t code = 1; // Code value

    for (const uint8_t* byte = (const uint8_t*)src; srcsize--; ++byte) {
        if (*byte) { // Byte not zero, write it
            *encode++ = *byte, ++code;
            if (encode - dest >= destsize)
                return destsize;
        }
        if (!*byte || code == 0xff) // Input is zero or block completed, restart
        {
            *codep = code, code = 1, codep = encode;
            if (!*byte || srcsize) {
                ++encode;
                if (encode - dest >= destsize)
                    return destsize;
            }
        }
    }
    *codep = code; // Write final code value

    return (size_t)(encode - dest);
}

int cobs_decode(void* dest, int destsize, const void* src, int srcsize)
{

    const uint8_t* byte = src; // Encoded input byte pointer
    uint8_t* decode = (uint8_t*)dest; // Decoded output byte pointer

    for (uint8_t code = 0xff, block = 0; byte < (uint8_t*)src + srcsize; --block) {
        if (block) { // Decode block byte
            *decode++ = *byte++;
            int bytes_written = (decode - (uint8_t*)dest);
            if (bytes_written >= destsize) {
                return bytes_written;
            }
        } else {
            if (code != 0xff) { // Encoded zero, write it
                *decode++ = 0;
                int bytes_written = (decode - (uint8_t*)dest);
                if (bytes_written >= destsize) {
                    return bytes_written;
                }
            }
            block = code = *byte++; // Next block length
            if (!code) // Delimiter code found
                break;
        }
    }
    return (size_t)(decode - (uint8_t*)dest);
}