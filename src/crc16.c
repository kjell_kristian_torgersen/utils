#include <stdbool.h>
#include <stdint.h>

uint8_t crc8_reverse(uint8_t b);

uint16_t crc16_reverse(uint16_t c)
{
    c = (c & 0xFF00) >> 8 | (c & 0x00FF) << 8;
    c = (c & 0xF0F0) >> 4 | (c & 0x0F0F) << 4;
    c = (c & 0xCCCC) >> 2 | (c & 0x3333) << 2;
    c = (c & 0xAAAA) >> 1 | (c & 0x5555) << 1;
    return c;
}

uint16_t crc16_update(uint16_t crc, uint16_t poly, uint8_t a)
{
    crc ^= (a << 8);
    for (int j = 0; j < 8; j++) {
        bool b = (crc & 0x8000);
        crc <<= 1;
        if (b) {
            crc ^= poly;
        }
    }

    return crc;
}

void crc16_fill_table(uint16_t* table, uint16_t poly, bool refin, bool refout)
{
    for (int i = 0; i < 256; i++) {
        uint8_t b = i;
        if (refin) {
            b = crc8_reverse(b);
        }
        table[i] = crc16_update(0, poly, b);
        if (refout) {
            table[i] = crc16_reverse(table[i]);
        }
    }
}

uint16_t crc16_use_table(const uint16_t* table, uint16_t crc, const void* buf, int count)
{
    const uint8_t* p = (const uint8_t*)buf;

    while (count--) {
        crc = (crc << 8) ^ table[((crc >> 8) ^ *p++)];
    }

    return crc;
}

uint16_t crc16_use_reftable(const uint16_t* table, uint16_t crc, const void* buf, int count)
{
    const uint8_t* p = (const uint8_t*)buf;

    while (count--) {
        crc = table[(crc ^ *p++) & 0xff] ^ (crc >> 8);
    }

    return crc;
}

uint16_t crc16_process(uint16_t poly, uint16_t crc, bool refin, const void* buf, int count)
{
    const uint8_t* p = (const uint8_t*)buf;

    while (count--) {
        uint16_t b = (*p++);
        if (refin) {
            b = crc8_reverse(b);
        }
        crc = crc16_update(crc, poly, b);
    }

    return crc;
}
