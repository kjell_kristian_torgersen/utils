#include <stdint.h>

#include "../filter_ma2.h"

void filter_ma2_init(filter_ma2_t* this, int32_t* storage, int32_t* acc, int order, int stages, int shift)
{
    this->x = storage;
    this->acc = acc;
    this->order = order;
    this->stages = stages;
    this->idx = 0;
    this->shift = shift;
    for (int i = 0; i < order * stages; i++) {
        this->x[i] = 0;
    }
    for (int i = 0; i < stages; i++) {
        this->acc[i] = 0;
    }
}

int32_t filter_ma2_process(filter_ma2_t* this, int32_t x)
{
    this->acc[0] -= this->x[this->idx];
    this->x[this->idx] = x;
    this->acc[0] += this->x[this->idx];

    for (int i = 1; i < this->stages; i++) {
        this->acc[i] -= this->x[this->idx + this->order * i];
        this->x[this->idx + this->order * i] = this->acc[i - 1] >> this->shift;
        this->acc[i] += this->x[this->idx + this->order * i];
    }

    this->idx = (this->idx + 1) % this->order;
    return this->acc[this->stages - 1];
}
