#include <stdbool.h>
#include <stdint.h>

uint8_t crc8_reverse(uint8_t b);

uint32_t crc32_reverse(uint32_t c)
{
    c = (c & 0xFFFF0000) >> 16 | (c & 0x0000FFFF) << 16;
    c = (c & 0xFF00FF00) >> 8 | (c & 0x00FF00FF) << 8;
    c = (c & 0xF0F0F0F0) >> 4 | (c & 0x0F0F0F0F) << 4;
    c = (c & 0xCCCCCCCC) >> 2 | (c & 0x33333333) << 2;
    c = (c & 0xAAAAAAAA) >> 1 | (c & 0x55555555) << 1;
    return c;
}

uint32_t crc32_update(uint32_t crc, uint32_t poly, uint8_t a)
{
    crc ^= (a << 24);
    for (int j = 0; j < 8; j++) {
        bool b = (crc & 0x80000000);
        crc <<= 1;
        if (b) {
            crc ^= poly;
        }
    }

    return crc;
}

uint32_t crc32_process(uint32_t poly, uint32_t crc, bool refin, const void* buf, int count)
{
    const uint8_t* p = (const uint8_t*)buf;

    while (count--) {
        uint8_t b = (*p++);
        if (refin) {
            b = crc8_reverse(b);
        }
        crc = crc32_update(crc, poly, b);
    }

    return crc;
}