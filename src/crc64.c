#include <stdint.h>

uint64_t crc64_update(uint64_t crc, uint64_t poly, uint8_t a)
{
    int i;

    crc ^= (uint64_t)a;
    for (i = 0; i < 8; ++i) {
        if (crc & 1)
            crc = (crc >> 1) ^ poly;
        else
            crc = (crc >> 1);
    }

    return crc;
}

uint64_t crc64_process(uint64_t crc, uint64_t poly, const void* buf, int count)
{
    const uint8_t* p = (const uint8_t*)buf;
    while (count--) {
        crc = crc64_update(crc, poly, *p++);
    }
    return crc;
}