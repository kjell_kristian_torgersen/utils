#include <stdint.h>

#include "../filter_ma.h"

void filter_ma16_init(filter_ma16_t* this, int16_t* storage, int n)
{
    this->x = storage;
    this->n = n;
    this->idx = 0;
    this->acc = 0;
    for (int i = 0; i < n; i++) {
        this->x[i] = 0;
    }
}

int16_t filter_ma16_process(filter_ma16_t* this, int16_t x)
{
    this->acc -= this->x[this->idx];
    this->x[this->idx] = x;
    this->acc += x;

    this->idx = (this->idx + 1) % this->n;
    return this->acc;
}

void filter_ma32_init(filter_ma32_t* this, int32_t* storage, int n)
{
    this->x = storage;
    this->n = n;
    this->idx = 0;
    this->acc = 0;
    for (int i = 0; i < n; i++) {
        this->x[i] = 0;
    }
}

int32_t filter_ma32_process(filter_ma32_t* this, int32_t x)
{
    this->acc -= this->x[this->idx];
    this->x[this->idx] = x;
    this->acc += x;

    this->idx = (this->idx + 1) % this->n;
    return this->acc;
}
