#include <stdbool.h>
#include <stdint.h>

uint8_t crc8_reverse(uint8_t b)
{
    b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
    b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
    b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
    return b;
}

uint16_t crc8_update(uint8_t crc, uint8_t poly, uint8_t a)
{
    crc ^= a;
    for (int j = 0; j < 8; j++) {
        bool b = (crc & 0x80);
        crc <<= 1;
        if (b) {
            crc ^= poly;
        }
    }

    return crc;
}

void crc8_fill_table(uint8_t* table, uint8_t poly, bool refin, bool refout)
{
    for (int i = 0; i < 256; i++) {
        uint8_t crc = i;
        if (refin) {
            crc = crc8_reverse(crc);
        }
        for (int j = 0; j < 8; j++) {
            bool b = (crc & 0x80);
            crc <<= 1;
            if (b) {
                crc ^= poly;
            }
        }
        if (refout) {
            crc = crc8_reverse(crc);
        }
        table[i] = crc;
    }
}

uint8_t crc8_use_table(const uint8_t* table, uint8_t crc, const void* buf, int count)
{
    const uint8_t* p = (const uint8_t*)buf;

    while (count--) {
        uint8_t b = *p++;
        /* if (refin) {
             b = crc8_reverse(b);
         }*/
        crc = table[(crc ^ b) & 0xFF];
    }
    /*if (refout) {
        crc = crc8_reverse(crc);
    }*/
    return crc;
}

uint8_t crc8_process(uint8_t poly, uint8_t crc, bool refin, const void* buf, int count)
{
    const uint8_t* p = (const uint8_t*)buf;

    while (count--) {
        uint16_t b = (*p++);
        if (refin) {
            b = crc8_reverse(b);
        }
        crc = crc8_update(crc, poly, b);
    }

    return crc;
}
