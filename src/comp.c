#include <stdio.h>
#include <stdlib.h>

#include "../comp.h"

#include "comp_priv.h"

static void write_nibble(comp_t* comp, uint8_t value)
{
    /* for (int i = 0; i < 4; i++) {
         printf("%i", (value >> (3 - i)) & 1);
     }
     printf(" ");*/

    comp->byte |= (value << (4 - comp->idx));
    comp->idx += 4;
    if (comp->idx == 8) {
        comp->writer(comp->writer_arg, &comp->byte, 1);
        comp->idx = 0;
        comp->byte = 0;
    }
}

uint16_t comp_hyst(uint16_t* last, uint16_t value)
{
    if (abs(*last - value) > 1) {
        *last = value;
    }
    return *last;
}

void comp_init(comp_t* comp, comp_write_t writer, void* arg)
{
    comp->writer = writer;
    comp->writer_arg = arg;
    comp->byte = 0;
    comp->idx = 0;
    comp->last_value = 0;
    comp->repeat = 0;
    /* write_nibble(comp, 1);
     write_nibble(comp, 2);
     write_nibble(comp, 3);
     write_nibble(comp, 4);
     write_nibble(comp, 5);
     write_nibble(comp, 6);*/
}

static void write_repeat(comp_t* comp)
{
    if (comp->repeat) {
        write_nibble(comp, COMP_REPEAT | (comp->repeat >> 4));
        write_nibble(comp, comp->repeat & 0xF);
        comp->repeat = 0;
    }
}

void comp_write(comp_t* comp, uint16_t value)
{
    int diff = value - comp->last_value;
    comp->last_value = value;

    if (diff == 0) {
        comp->repeat++;
        if (comp->repeat == 62) {
            write_repeat(comp);
        }
    } else if (diff >= -4 && diff <= 3) {
        write_repeat(comp);
        write_nibble(comp, COMP_DIFF | (diff + 4));
    } else {
        write_repeat(comp);
        write_nibble(comp, COMP_SET | (value >> 12));
        write_nibble(comp, (value >> 8) & 0x0f);
        write_nibble(comp, (value >> 4) & 0x0f);
        write_nibble(comp, value & 0x0f);
    }
}

void comp_finalize(comp_t* comp)
{
    write_repeat(comp);
    if (comp->idx) {
        comp->byte |= 0x0F;
        comp->writer(comp->writer_arg, &comp->byte, 1);
    }
}