#include <stdint.h>
#include <string.h>

#include "../random.h"

static uint32_t state = 0;

void random_init(uint32_t seed)
{
    state = seed;
}

void random_get(void* buf, uint32_t size)
{
    int n = size / 4;
    int rest = size & 3;
    uint32_t* data = buf;

    for (int i = 0; i < n; i++) {
        state += 0xe120fc15;
        uint64_t a = (uint64_t)state * 0x4a39b70d;
        uint32_t b = (a >> 32) ^ a;
        a = (uint64_t)b * 0x12fad5c9;
        data[i] = (a >> 32) ^ a;
    }

    if (rest) {
        uint32_t tmp = random_u32();
        memcpy(&data[n], &tmp, rest);
    }
}

uint8_t random_u8(void)
{
    return random_u32();
}

uint16_t random_u16(void)
{
    return random_u32();
}

uint32_t random_u32(void)
{
    state += 0xe120fc15;
    uint64_t a = (uint64_t)state * 0x4a39b70d;
    uint32_t b = (a >> 32) ^ a;
    a = (uint64_t)b * 0x12fad5c9;
    uint32_t c = (a >> 32) ^ a;
    return c;
}

uint64_t random_u64(void)
{
    return random_u32() | ((uint64_t)random_u32() << 32);
}
