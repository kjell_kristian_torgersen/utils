#ifndef D9DA80A7_5C95_4B7E_8804_E2693526DB2E
#define D9DA80A7_5C95_4B7E_8804_E2693526DB2E

typedef struct filter_ma16 {
    int16_t* x;
    int idx;
    int n;
    int16_t acc;
} filter_ma16_t;

typedef struct filter_ma32 {
    int32_t* x;
    int idx;
    int n;
    int32_t acc;
} filter_ma32_t;

void filter_ma16_init(filter_ma16_t* this, int16_t* storage, int n);
int16_t filter_ma16_process(filter_ma16_t* this, int16_t x);

void filter_ma32_init(filter_ma32_t* this, int32_t* storage, int n);
int32_t filter_ma32_process(filter_ma32_t* this, int32_t x);

#endif /* D9DA80A7_5C95_4B7E_8804_E2693526DB2E */
