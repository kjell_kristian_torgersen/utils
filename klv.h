#ifndef B116A800_233E_40B4_9BD1_4AB20DA906A1
#define B116A800_233E_40B4_9BD1_4AB20DA906A1

#include <stdint.h>

#include "api/ret.h"

typedef struct flash flash_t;

typedef struct klv {
    flash_t* flash; // flash device
    uint32_t addr_a; // address of block A
    uint32_t addr_b; // address of block B
    uint32_t block_size; // in bytes
    uint32_t entries[256]; // address of all entries in the klv
    uint32_t write_idx;
    // uint32_t read_idx;
} klv_t;

/** \brief Initialize a key length value structure */
void klv_init(klv_t* klv, flash_t* flash, uint32_t addr_a, uint32_t addr_b, uint32_t block_size);

/** \brief Write or update an entry
 * \param klv The key length value structure
 * \param key The key to write
 * \param len The length of the value
 * \param value The value to write
 * \return A ret_t indicating success or failure
 */
ret_t klv_write(klv_t* klv, uint8_t key, const void* data, uint8_t len);

/** \brief Read an entry
 * \param klv The key length value structure
 * \param key The key to read
 * \param len The length of the value
 * \param value The value to read
 * \return Number of bytes read, 0 if not found or error
 */
uint8_t klv_read(klv_t* klv, uint8_t key, void* data, uint8_t len);

/** \brief Erase an entry
 * \param klv The key length value structure
 * \param key The key to delete
 * \return A ret_t indicating success or failure
 */
ret_t klv_erase(klv_t* klv, uint8_t key);

#endif /* B116A800_233E_40B4_9BD1_4AB20DA906A1 */
