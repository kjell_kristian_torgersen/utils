#ifndef ADC7D979_BB2C_4F06_9C97_09FD720DC742
#define ADC7D979_BB2C_4F06_9C97_09FD720DC742

#include <stdbool.h>
#include <stdint.h>

typedef int (*decomp_read_t)(void* arg, void* buf, int size);

typedef struct decomp {
    decomp_read_t reader;
    void* reader_arg;
    uint16_t value;
    uint16_t last_value;
    uint8_t repeat;
    uint8_t byte;
    uint8_t idx;
} decomp_t;

void decomp_init(decomp_t* decomp, decomp_read_t reader, void* arg);
bool decomp_read(decomp_t* decomp, uint16_t* value);

#endif /* ADC7D979_BB2C_4F06_9C97_09FD720DC742 */
