#ifndef FBB7B8F2_47AE_4975_A8B0_2A6F8F309B91
#define FBB7B8F2_47AE_4975_A8B0_2A6F8F309B91

#include <stdint.h>

//#include "api/io.h"

typedef struct chacha20 {
    uint32_t x[16];
    uint8_t output[64];
    uint8_t output_idx;
} chacha20_t;

struct io* io_chacha20_init(struct io* next, chacha20_t* chacha20);
/** \brief Initialize the chacha20 cipher.
 * \param *this The chacha20 cipher instance to initialize.
 * \param *key The key to use. Must be 256 bits/32 bytes long.
 */
void chacha20_init(chacha20_t* this, const uint8_t* key);
/** \brief Set nonce and counter for the chacha20 cipher. counter can be used to random access the keystream, for example when using this for file, flash or disk encryption.
 * \param *this The chacha20 cipher instance to set nonce and counter for.
 * \param nonce The nonce to use.
 * \param counter The counter to use.
 */
void chacha20_set_nonce(chacha20_t* this, uint64_t nonce, uint64_t counter);
/** \brief Process data with the chacha20 cipher.
 * \param *this The chacha20 cipher instance to process data with.
 * \param *dest The destination buffer to write the processed data to.
 * \param *src The source buffer to read the data from.
 * \param count The number of bytes to process. Can be any positive number as long as dest[count] and src[count] are valid.
 * \sa chacha20_init
 * \sa chacha20_set_nonce
 */
int chacha20_process(chacha20_t* this, uint8_t* dest, const uint8_t* src, int count);
#endif /* FBB7B8F2_47AE_4975_A8B0_2A6F8F309B91 */
