#ifndef C2D87090_C67A_4952_ADD8_AF67555285D2
#define C2D87090_C67A_4952_ADD8_AF67555285D2

#include <stdint.h>

typedef int (*comp_write_t)(void* arg, const void* data, int count);

typedef struct comp {
    comp_write_t writer;
    void* writer_arg;
    uint16_t last_value;
    uint8_t byte;
    uint8_t idx;
    uint8_t repeat;
} __attribute__((__packed__)) comp_t;

uint16_t comp_hyst(uint16_t* last, uint16_t value);
void comp_init(comp_t* comp, comp_write_t writer, void* arg);
void comp_write(comp_t* comp, uint16_t value);
void comp_finalize(comp_t* comp);

#endif /* C2D87090_C67A_4952_ADD8_AF67555285D2 */
