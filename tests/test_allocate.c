#include <stdint.h>

#include "tests.h"

#include "api/allocate.h"

void test_allocate(void)
{
    uint32_t heap[3];
    allocate_init(heap, sizeof(heap));
    ASSERT(allocate_usage() == 0);

    // check that allocation works
    uint32_t* p0 = allocate(4);
    ASSERT(p0 - heap == 0);
    ASSERT(allocate_usage() == 4);

    // check that aligment works
    uint32_t* p1 = allocate(3);
    ASSERT(allocate_usage() == 8);
    uint32_t* p2 = allocate(4);
    ASSERT(allocate_usage() == 12);
    ASSERT(p1 - heap == 4);

    ASSERT(p2 - heap == 8);

    // check that allocation fails since we only have 12 bytes available in the heap
    uint32_t* p3 = allocate(1);
    ASSERT(p3 == NULL);
}