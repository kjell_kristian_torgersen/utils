#include "../linbuf.h"

#include "tests.h"

void test_linbuf(void)
{
    linbuf_t lb;
    char storage[8];
    linbuf_init(&lb, storage, sizeof(storage));

    // Verify initialization is OK
    ASSERT(lb.storage == (void*)storage);
    ASSERT(lb.storage_size == sizeof(storage));
    ASSERT(lb.used_bytes == 0);

    // Verify a typical write
    ASSERT(linbuf_write(&lb, "hello", 5) == 5);
    ASSERT(linbuf_len(&lb) == 5);
    ASSERT(linbuf_capacity(&lb) == sizeof(storage));
    ASSERT(linbuf_free_space(&lb) == sizeof(storage) - 5);
    ASSERT(array_cmp(linbuf_get(&lb), "hello", 5));

    // Verify clearing
    linbuf_clear(&lb);
    ASSERT(linbuf_len(&lb) == 0);
    ASSERT(linbuf_capacity(&lb) == sizeof(storage));
    ASSERT(linbuf_free_space(&lb) == sizeof(storage));

    // Verify that writing to many bytes at once is gracefully handled
    ASSERT(linbuf_write(&lb, "0123456789", 10) == sizeof(storage));
    ASSERT(linbuf_len(&lb) == sizeof(storage));
    ASSERT(linbuf_capacity(&lb) == sizeof(storage));
    ASSERT(linbuf_free_space(&lb) == 0);
    ASSERT(array_cmp(linbuf_get(&lb), "01234567", 8));
}
