#include "tests.h"

#include "../chacha20.h"

void test_chacha20(void)
{
    chacha20_t chacha;
    uint8_t key[32];

    uint8_t m[1024];
    uint8_t c[1024];

    for (int i = 0; i < 32; i++)
        key[i] = 0;

    for (int i = 0; i < 50; i++)
        m[i] = 0;

    chacha20_init(&chacha, key);
    chacha20_set_nonce(&chacha, 0x0000000000000000, 0);
    for (int i = 0; i < 20; i++) {
        chacha20_process(&chacha, &c[i], &m[i], 1);
    }
    // chacha20_process(&chacha, &c[10], &m[10], 10);
    uint8_t output[20] = { 0x76, 0xb8, 0xe0, 0xad, 0xa0, 0xf1, 0x3d, 0x90, 0x40, 0x5d, 0x6a, 0xe5, 0x53, 0x86, 0xbd, 0x28, 0xbd, 0xd2, 0x19, 0xb8 };
    ASSERT(array_cmp(c, output, 20));

    /*	uint8_t *in  = malloc(16000000);
            uint8_t *out = malloc(16000000);
            clock_t begin, end;
            double time_spent;

            begin = clock();
            for(int i = 0; i < 100; i++) {
                    chacha20_process(&chacha, out, in, 16000000);
            }
            end	   = clock();
            time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
            printf("chacha20_process: %f %f MB/s\n", time_spent,  100.0*16000000.0/1024.0/1024.0 / time_spent);

            free(in);
            free(out);*/
}