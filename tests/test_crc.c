#include <stdint.h>
#include <stdio.h>

#include "../crc16.h"
#include "../crc32.h"
#include "../crc64.h"
#include "../crc8.h"

#include "tests.h"

typedef struct crc_test {
    const char* name;
    uint32_t check;
    uint32_t poly;
    uint32_t init;
    bool ref_in;
    bool ref_out;
    uint32_t xor_out;
} crc_test_t;

static const crc_test_t crc8_tests[] = {
    { "CRC-8", 0xF4, 0x07, 0x00, false, false, 0x00 },
    { "CRC-8/CDMA2000", 0xDA, 0x9B, 0xFF, false, false, 0x00 },
    { "CRC-8/DARC", 0x15, 0x39, 0x00, true, true, 0x00 },
    { "CRC-8/DVB-S2", 0xBC, 0xD5, 0x00, false, false, 0x00 },
    { "CRC-8/EBU", 0x97, 0x1D, 0xFF, true, true, 0x00 },
    { "CRC-8/I-CODE", 0x7E, 0x1D, 0xFD, false, false, 0x00 },
    { "CRC-8/ITU", 0xA1, 0x07, 0x00, false, false, 0x55 },
    { "CRC-8/MAXIM", 0xA1, 0x31, 0x00, true, true, 0x00 },
    { "CRC-8/ROHC", 0xD0, 0x07, 0xFF, true, true, 0x00 },
    { "CRC-8/WCDMA", 0x25, 0x9B, 0x00, true, true, 0x00 },
};

#define CRC8_TESTS (sizeof(crc8_tests) / sizeof(crc_test_t))

static const crc_test_t crc16_tests[] = {
    { "CRC-16/CCITT-FALSE", 0x29B1, 0x1021, 0xFFFF, false, false, 0x0000 },
    { "CRC-16/ARC", 0xBB3D, 0x8005, 0x0000, true, true, 0x0000 },
    { "CRC-16/AUG-CCITT", 0xE5CC, 0x1021, 0x1D0F, false, false, 0x0000 },
    { "CRC-16/BUYPASS", 0xFEE8, 0x8005, 0x0000, false, false, 0x0000 },
    { "CRC-16/CDMA2000", 0x4C06, 0xC867, 0xFFFF, false, false, 0x0000 },
    { "CRC-16/DDS-110", 0x9ECF, 0x8005, 0x800D, false, false, 0x0000 },
    { "CRC-16/DECT-R", 0x007E, 0x0589, 0x0000, false, false, 0x0001 },
    { "CRC-16/DECT-X", 0x007F, 0x0589, 0x0000, false, false, 0x0000 },
    { "CRC-16/DNP", 0xEA82, 0x3D65, 0x0000, true, true, 0xFFFF },
    { "CRC-16/EN-13757", 0xC2B7, 0x3D65, 0x0000, false, false, 0xFFFF },
    { "CRC-16/GENIBUS", 0xD64E, 0x1021, 0xFFFF, false, false, 0xFFFF },
    { "CRC-16/MAXIM", 0x44C2, 0x8005, 0x0000, true, true, 0xFFFF },
    { "CRC-16/MCRF4XX", 0x6F91, 0x1021, 0xFFFF, true, true, 0x0000 },
    { "CRC-16/RIELLO", 0x63D0, 0x1021, 0xB2AA, true, true, 0x0000 },
    { "CRC-16/T10-DIF", 0xD0DB, 0x8BB7, 0x0000, false, false, 0x0000 },
    { "CRC-16/TELEDISK", 0x0FB3, 0xA097, 0x0000, false, false, 0x0000 },
    { "CRC-16/TMS37157", 0x26B1, 0x1021, 0x89EC, true, true, 0x0000 },
    { "CRC-16/USB", 0xB4C8, 0x8005, 0xFFFF, true, true, 0xFFFF },
    { "CRC-A", 0xBF05, 0x1021, 0xC6C6, true, true, 0x0000 },
    { "CRC-16/KERMIT", 0x2189, 0x1021, 0x0000, true, true, 0x0000 },
    { "CRC-16/MODBUS", 0x4B37, 0x8005, 0xFFFF, true, true, 0x0000 },
    { "CRC-16/X-25", 0x906E, 0x1021, 0xFFFF, true, true, 0xFFFF },
    { "CRC-16/XMODEM", 0x31C3, 0x1021, 0x0000, false, false, 0x0000 },
};

#define CRC16_TESTS (sizeof(crc16_tests) / sizeof(crc_test_t))

static const crc_test_t crc32_tests[] = {
    { "CRC-32",
        0xCBF43926, 0x04C11DB7, 0xFFFFFFFF, true, true, 0xFFFFFFFF },
    { "CRC-32/BZIP2",
        0xFC891918, 0x04C11DB7, 0xFFFFFFFF, false, false, 0xFFFFFFFF },
    { "CRC-32C",
        0xE3069283, 0x1EDC6F41, 0xFFFFFFFF, true, true, 0xFFFFFFFF },
    { "CRC-32D",
        0x87315576, 0xA833982B, 0xFFFFFFFF, true, true, 0xFFFFFFFF },
    { "CRC-32/JAMCRC",
        0x340BC6D9, 0x04C11DB7, 0xFFFFFFFF, true, true, 0x00000000 },
    { "CRC-32/MPEG-2",
        0x0376E6E7, 0x04C11DB7, 0xFFFFFFFF, false, false, 0x00000000 },
    { "CRC-32/POSIX",
        0x765E7680, 0x04C11DB7, 0x00000000, false, false, 0xFFFFFFFF },
    { "CRC-32Q",
        0x3010BF7F, 0x814141AB, 0x00000000, false, false, 0x00000000 },
    { "CRC-32/XFER",
        0xBD0BE338, 0x000000AF, 0x00000000, false, false, 0x00000000 },
};

#define CRC32_TESTS (sizeof(crc32_tests) / sizeof(crc_test_t))

static void test_crc8(void)
{
    const uint8_t data[] = "123456789";
    uint8_t table[256];

    for (int i = 0; i < (int)CRC8_TESTS; i++) {
        uint8_t crc = crc8_process(crc8_tests[i].poly, crc8_tests[i].init, crc8_tests[i].ref_in, data, 9);
        if (crc8_tests[i].ref_out) {
            crc = crc8_reverse(crc);
        }
        crc ^= crc8_tests[i].xor_out;
        ASSERT(crc == crc8_tests[i].check);
        if (crc == crc8_tests[i].check) {
            // printf("NAIVE %s: OK\n", crc8_tests[i].name);
        } else {
            printf("NAIVE %s: FAILED expected %x but got %x\n", crc8_tests[i].name, crc8_tests[i].check, crc);
        }
    }
    for (int i = 0; i < (int)CRC8_TESTS; i++) {
        crc8_fill_table(table, crc8_tests[i].poly, crc8_tests[i].ref_in, crc8_tests[i].ref_out);
        uint8_t crc = crc8_tests[i].init;
        for (int j = 0; j < 9; j++) {
            crc = crc8_use_table(table, crc, &data[j], 1);
        }
        crc ^= crc8_tests[i].xor_out;
        if (crc == crc8_tests[i].check) {
            // printf("TABLE %s: OK\n", crc8_tests[i].name);
        } else {
            printf("TABLE %s: FAILED expected %x but got %x\n", crc8_tests[i].name, crc8_tests[i].check, crc);
        }
    }
}

static void test_crc16(void)
{
    const uint8_t data[] = "123456789";
    for (int i = 0; i < (int)CRC16_TESTS; i++) {
        uint16_t crc = crc16_process(crc16_tests[i].poly, crc16_tests[i].init, crc16_tests[i].ref_in, data, 9);
        // ASSERT(crc == crc16_tests[i].check);
        if (crc16_tests[i].ref_out) {
            crc = crc16_reverse(crc);
        }
        crc ^= crc16_tests[i].xor_out;
        if (crc == crc16_tests[i].check) {
            // printf("%s: OK\n", crc16_tests[i].name);
        } else {
            printf("%s: FAILED expected %x but got %x\n", crc16_tests[i].name, crc16_tests[i].check, crc);
        }
    }

    for (int j = 0; j < (int)CRC16_TESTS; j++) {
        uint16_t crc = crc16_tests[j].init;
        for (int i = 0; i < 9; i++) {
            crc = crc16_process(crc16_tests[j].poly, crc, crc16_tests[j].ref_in, &data[i], 1);
        }
        if (crc16_tests[j].ref_out) {
            crc = crc16_reverse(crc);
        }
        crc ^= crc16_tests[j].xor_out;
        if (crc == crc16_tests[j].check) {
            // printf("%s: OK\n", crc16_tests[j].name);
        } else {
            printf("%s: FAILED expected %x but got %x\n", crc16_tests[j].name, crc16_tests[j].check, crc);
        }
        // ASSERT(crc == crc16_tests[j].check);
    }
    for (int j = 0; j < (int)CRC16_TESTS; j++) {
        uint16_t table[256];
        crc16_fill_table(table, crc16_tests[j].poly, crc16_tests[j].ref_in, crc16_tests[j].ref_out);
        uint16_t crc = crc16_tests[j].init;
        for (int i = 0; i < 9; i++) {
            if (crc16_tests[j].ref_out) {
                crc = crc16_use_reftable(table, crc, &data[i], 1);
            } else {
                crc = crc16_use_table(table, crc, &data[i], 1);
            }
        }
        /*if (crc16_tests[j].ref_out) {
            crc = crc8_reverse(crc);
        }*/
#if 0
        crc ^= crc16_tests[j].xor_out;
        if (crc == crc16_tests[j].check) {
            // printf("TABLE %s: OK\n", crc16_tests[j].name);
        } else {
            printf("TABLE %s: FAILED expected %x but got %x\n", crc16_tests[j].name, crc16_tests[j].check, crc);
        }
#endif
    }
}

static void test_crc32(void)
{
    const uint8_t data[] = "123456789";
    for (int i = 0; i < (int)CRC32_TESTS; i++) {
        uint32_t crc = crc32_process(crc32_tests[i].poly, crc32_tests[i].init, crc32_tests[i].ref_in, data, 9);
        // ASSERT(crc == crc16_tests[i].check);
        if (crc32_tests[i].ref_out) {
            crc = crc32_reverse(crc);
        }
        crc ^= crc32_tests[i].xor_out;
        if (crc == crc32_tests[i].check) {
            //   printf("%s: OK\n", crc32_tests[i].name);
        } else {
            printf("%s: FAILED expected %x but got %x\n", crc32_tests[i].name, crc32_tests[i].check, crc);
        }
    }
#if 0
    for (int j = 0; j < (int)CRC32_TESTS; j++) {
        uint32_t crc = crc32_tests[j].init;
        for (int i = 0; i < 9; i++) {
            crc = crc32_process(crc16_tests[j].poly, crc, crc16_tests[j].ref_in, &data[i], 1);
        }
        if (crc16_tests[j].ref_out) {
            crc = crc16_reverse(crc);
        }
        crc ^= crc16_tests[j].xor_out;
        if (crc == crc16_tests[j].check) {
            printf("%s: OK\n", crc16_tests[j].name);
        } else {
            printf("%s: FAILED expected %x but got %x\n", crc16_tests[j].name, crc16_tests[j].check, crc);
        }
    }
#endif
}

void test_crc(void)
{
    test_crc8();
    test_crc16();
    test_crc32();

    /*uint8_t table[256];
    crc8_fill_table(table, 0x31, true, true);

    for (int i = 0; i < 256; i++) {
        printf("%02x ", (table[i]));
        if ((i & 15) == 15) {
            printf("\n");
        }
    }*/
}