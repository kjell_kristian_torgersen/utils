#ifndef B60E9B61_8783_401C_820B_29786C08C50D
#define B60E9B61_8783_401C_820B_29786C08C50D

#include <stdbool.h>
#include <stdio.h>

#define ASSERT(x)                                                                     \
    {                                                                                 \
        tests++;                                                                      \
        if (!(x)) {                                                                   \
            fprintf(stderr, "%s:%i: Assertion %s failed!\n", __FILE__, __LINE__, #x); \
            for_breakpoint();                                                         \
            failed++;                                                                 \
        }                                                                             \
    }

extern int failed;
extern int tests;

void for_breakpoint(void);
bool array_cmp(const void* aa, const void* bb, int count);

#endif /* B60E9B61_8783_401C_820B_29786C08C50D */
