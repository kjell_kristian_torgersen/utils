#ifndef C13DF93C_3A8C_4168_BFE6_CB882F9B5C57
#define C13DF93C_3A8C_4168_BFE6_CB882F9B5C57

#include <stdint.h>

struct flash {
    uint32_t capacity;
    uint32_t page_size;
    uint8_t* data;
};

#endif /* C13DF93C_3A8C_4168_BFE6_CB882F9B5C57 */
