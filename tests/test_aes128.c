#include <string.h>

#include "tests.h"

#include "../aes128.h"

void test_aes128()
{
    const uint8_t plain_text[] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff };
    const uint8_t cipher_key[] = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f };
    const uint8_t cipher_text[] = { 0x69, 0xc4, 0xe0, 0xd8, 0x6a, 0x7b, 0x04, 0x30, 0xd8, 0xcd, 0xb7, 0x80, 0x70, 0xb4, 0xc5, 0x5a };

    uint8_t cipher_text_out[16];
    uint8_t plain_text_out[16];

    aes128_t aes128;

    aes128_init(&aes128, cipher_key);

    ASSERT(aes128_encrypt(&aes128, cipher_text_out, plain_text, 16) == 16);
    ASSERT(memcmp(cipher_text_out, cipher_text, 16) == 0);

    ASSERT(aes128_decrypt(&aes128, plain_text_out, cipher_text, 16) == 16);
    ASSERT(memcmp(plain_text_out, plain_text, 16) == 0);

    /*uint8_t *in  = malloc(16000000);
    uint8_t *out = malloc(16000000);

    clock_t begin = clock();
    for (int i = 0; i < 10; i++) {
            ASSERT(aes128_encrypt(&aes128, out, in, 16000000) == 16000000);
    }
    clock_t end	   = clock();
    double	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("aes128_encrypt: %f %f MB/s\n", time_spent, 10.0*16000000.0/1024.0/1024.0 / time_spent);
    free(in);
    free(out);*/
}

void test_aes128_ctr(void)
{
    aes128_t aes;
    uint8_t key[16] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
    uint8_t input[34] = { 0x1, 0x2, 0x3 };
    uint8_t output[sizeof(input)];
    uint8_t decoded[sizeof(input)];

    aes128_init(&aes, key);
    ASSERT(aes128_ctr(&aes, 0xfedcba9876543210, output, input, sizeof(input)) == sizeof(input));
    ASSERT(aes128_ctr(&aes, 0xfedcba9876543210, decoded, output, sizeof(output)) == sizeof(output));
    // misc_showArr(output, sizeof(output));
    // misc_showArr(decoded, sizeof(decoded));
}