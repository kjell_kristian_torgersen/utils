#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "ram_flash.h"

#include "api/flash.h"
#include "api/ret.h"

flash_t* ram_flash_init(void* storage, int capacity, int page_size, bool erase)
{
    flash_t* this = malloc(sizeof(flash_t));
    if (this != NULL) {
        this->capacity = capacity;
        this->page_size = page_size;
        this->data = storage;
        if (erase) {
            for (int i = 0; i < capacity; i++) {
                this->data[i] = 0xFF;
            }
        }
    }
    return this;
}

ret_t flash_set_callback(flash_t* this, flash_done_t cb, void* arg)
{
    (void)this;
    (void)cb;
    (void)arg;
    return RET_NOT_IMPLEMENTED;
}

ret_t flash_read(flash_t* this, uint32_t addr, void* data, unsigned count)
{
    memcpy(data, this->data + addr, count);
    return count;
}

ret_t flash_write(flash_t* this, uint32_t addr, const void* data, unsigned count)
{
    for (int i = 0; i < (int)count; i++) {
        this->data[addr + i] &= ((const uint8_t*)data)[i];
    }
    return count;
}

ret_t flash_erase(flash_t* this, uint32_t addr, uint32_t count)
{
    uint32_t pages = count / this->page_size;
    if (addr & (this->page_size - 1)) {
        return RET_INVALID_ARG;
    }
    for (uint32_t i = 0; i < pages; i++) {
        for (uint32_t j = 0; j < this->page_size; j++) {
            this->data[addr + i * this->page_size + j] = 0xFF;
        }
    }
    return pages * this->page_size;
}

ret_t flash_command(flash_t* this, flash_commands_t command, void* arg)
{
    (void)this;
    (void)command;
    (void)arg;
    return RET_NOT_IMPLEMENTED;
}

ret_t flash_close(flash_t* this)
{
    if (this == NULL) {
        return RET_NULL_POINTER;
    }
    if (this != NULL) {
        free(this);
    }
    return RET_OK;
}
