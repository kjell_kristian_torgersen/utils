#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "misc.h"

void test_ringbuf(void);
void test_linbuf(void);
void test_cmd(void);
void test_cobs(void);
void test_aes128(void);
void test_aes128_ctr(void);
void test_chacha20(void);
void test_ptimer(void);
void test_datareceived_event(void);
void test_crc(void);
void test_filter_ma(void);
void test_ram_flash(void);
void test_klv(void);
void test_comp(void);
void test_allocate(void);

int failed = 0;
int tests = 0;

void for_breakpoint(void)
{
    volatile int x = 7;
    x++;
}

bool array_cmp(const void* aa, const void* bb, int count)
{
    const uint8_t* a = aa;
    const uint8_t* b = bb;
    for (int i = 0; i < count; i++) {
        if (a[i] != b[i]) {
            printf("a:\n");
            misc_showArr(aa, count);
            printf("b:\n");
            misc_showArr(bb, count);
            printf("a[%x] == %x, b[%x] == %x\n", i, a[i], i, b[i]);
            return false;
        }
    }
    return true;
}

int main(int argc, char** argv)
{
    (void)argc;
    (void)argv;

    test_ringbuf();
    test_linbuf();
    test_cmd();
    test_cobs();
    test_aes128();
    test_aes128_ctr();
    test_chacha20();
    test_ptimer();
    test_datareceived_event();
    test_crc();
    test_filter_ma();
    test_ram_flash();
    test_klv();
    test_comp();
    test_allocate();

    printf("%i failed, %i passed and %i tests total\n", failed, tests - failed, tests);
    return failed;
}
