#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "tests.h"

#include "../datareceived_event.h"

static void* e1_userdata = (void*)1;
static uint8_t e1_data[5] = { 0 };
static int e1_size = 0;
static void e1_callback(void* userdata, const void* data, int size)
{
    e1_userdata = userdata;
    memcpy(e1_data, data, size);
    e1_size = size;
}

static void* e2_userdata = NULL;
static uint8_t e2_data[5] = { 0 };
static int e2_size = 0;
static void e2_callback(void* userdata, const void* data, int size)
{
    e2_userdata = userdata;
    memcpy(e2_data, data, size);
    e2_size = size;
}

void test_datareceived_event(void)
{
    datareceived_event_t* head = NULL;
    datareceived_event_t e1, e2;

    ASSERT(head == NULL);

    datareceived_event_subscribe(&head, &e1, e1_callback, NULL);
    // check that everything is initialized as expected
    ASSERT(head == &e1);
    ASSERT(e1.callback == e1_callback);
    ASSERT(e1.userdata == NULL);
    ASSERT(e1.next == NULL);

    // publish to only e1 (e2 hasn't subscribed yet)
    datareceived_event_publish(head, "hello", 5);
    ASSERT(e1_userdata == NULL);
    ASSERT(e1_size == 5);
    ASSERT(memcmp(e1_data, "hello", 5) == 0);

    // verify that e2 is not called
    ASSERT(e2_userdata != (void*)1);
    ASSERT(e2_size == 0);
    ASSERT(memcmp(e2_data, "hello", 5) != 0);

    // subscribe e2
    datareceived_event_subscribe(&head, &e2, e2_callback, (void*)1);
    // verify that the content of e2 also is correct
    ASSERT(head == &e2);
    ASSERT(e2.callback == e2_callback);
    ASSERT(e2.userdata == (void*)1);
    ASSERT(e2.next == &e1);

    // publish to both e1 and e2
    datareceived_event_publish(head, "hi", 2);

    // verify that both callback has received data
    ASSERT(e1_userdata == NULL);
    ASSERT(e1_size == 2);
    ASSERT(memcmp(e1_data, "hi", 2) == 0);

    ASSERT(e2_userdata == (void*)1);
    ASSERT(e2_size == 2);
    ASSERT(memcmp(e2_data, "hi", 2) == 0);

    // disable e1 by setting it's callback to NULL
    datareceived_event_subscribe(&head, &e1, NULL, (void*)2);

    // verify that e1 is not called anymore
    e1_userdata = NULL;
    datareceived_event_publish(head, "bye", 3);

    ASSERT(e1_userdata == NULL);
    ASSERT(e1_size != 3);
    ASSERT(memcmp(e1_data, "bye", 3) != 0);

    ASSERT(e2_userdata == (void*)1);
    ASSERT(e2_size == 3);
    ASSERT(memcmp(e2_data, "bye", 3) == 0);
}