#include <stdlib.h>
#include <string.h>

#include "../klv.h"

#include "api/flash.h"

#include "tests.h"

extern flash_t* ram_flash_init(void* storage, int capacity, int page_size, bool erase);
extern void display_flash(flash_t* flash);
#define FLASH_SIZE 256
#define FLASH_PAGE_SIZE 32

void test_klv_init(void)
{
    // these tests fails if the padding is other than 2 bytes, TODO: make arrays for different padding sizes
    uint8_t storage[] = {
        0x01, 0x02, 0xAA, 0xAA, 0x00, 0x1B, 0x02, 0x02, 0xBB, 0xBB, 0x00, 0x36, 0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
    };

    flash_t* flash = ram_flash_init(storage, sizeof(storage), sizeof(storage) / 2, false);
    klv_t klv;
    klv_init(&klv, flash, 0, sizeof(storage) / 2, sizeof(storage) / 2);
    ASSERT(klv.write_idx == 12);
    char buf[5];
    ASSERT(klv_read(&klv, 1, buf, 2) == 2);
    ASSERT(memcmp(buf, "\xaa\xaa", 2) == 0);
    ASSERT(klv_read(&klv, 2, buf, 2) == 2);
    ASSERT(memcmp(buf, "\xbb\xbb", 2) == 0);
}

void test_klv_read_write(void)
{
    char storage[FLASH_SIZE];
    uint8_t buf[5];
    klv_t klv;
    flash_t* flash = ram_flash_init(storage, FLASH_SIZE, FLASH_SIZE / 2, true);
    klv_init(&klv, flash, 0, FLASH_SIZE / 2, FLASH_SIZE / 2);
    ASSERT(klv.flash == flash);
    ASSERT(klv.addr_a == 0);
    ASSERT(klv.addr_b == FLASH_SIZE / 2);
    ASSERT(klv.block_size == FLASH_SIZE / 2);
    ASSERT(klv.write_idx == 0x0);
    // display_flash(flash);

    // write new entry
    ASSERT(klv_write(&klv, 1, "Hello", 5) == 5);
    // display_flash(flash);

    // read back entry
    ASSERT(klv_read(&klv, 1, buf, 5) == 5);
    ASSERT(memcmp(buf, "Hello", 5) == 0);

    // erase entry
    ASSERT(klv_erase(&klv, 1) == RET_OK);
    // display_flash(flash);
    // verify that the entry is gone and we read 0 bytes
    ASSERT(klv_read(&klv, 1, buf, 5) == 0);

    // write to deleted entry
    ASSERT(klv_write(&klv, 1, "Hello", 5) == 5);
    // display_flash(flash);
    ASSERT(klv_read(&klv, 1, buf, 5) == 5);
    ASSERT(array_cmp(buf, "Hello", 5));

    // update entry without delete first
    ASSERT(klv_write(&klv, 1, "World", 5) == 5);

    ASSERT(klv_read(&klv, 1, buf, 5) == 5);
    ASSERT(array_cmp(buf, "World", 5));

    // test moving data to next block (and delete old one)
    for (int i = 0; i < 10 * FLASH_SIZE / (int)sizeof(int); i++) {
        int j = 0;

        klv_write(&klv, 2, &i, sizeof(int));
        klv_read(&klv, 2, &j, sizeof(int));
        if (i != j) {
            ASSERT(i == j);
            printf("i: %d, j: %d\n", i, j);
            break;
        }
    }
}

void test_klv_stress(void)
{
    char storage[FLASH_SIZE];

    uint16_t fasit[] = { 0xAAAA, 0xBBBB, 0xCCCC, 0xDDDD, 0xEEEE };
    uint16_t tall;
    klv_t klv;
    flash_t* flash = ram_flash_init(storage, FLASH_SIZE, FLASH_SIZE / 2, true);
    klv_init(&klv, flash, 0, FLASH_SIZE / 2, FLASH_SIZE / 2);
    for (int i = 0; i < 1000; i++) {
        int r = rand() % 5;
        if (klv_write(&klv, r + 1, &fasit[r], sizeof(fasit[r])) != sizeof(fasit[r])) {
            ASSERT(false);
        }
    }

    for (int i = 0; i < 5; i++) {
        ASSERT(klv_read(&klv, i + 1, &tall, sizeof(tall)) == sizeof(tall));
        ASSERT(tall == fasit[i]);
    }
    // init again to make sure we can read from the same flash
    klv_init(&klv, flash, 0, FLASH_SIZE / 2, FLASH_SIZE / 2);
    for (int i = 0; i < 5; i++) {
        ASSERT(klv_read(&klv, i + 1, &tall, sizeof(tall)) == sizeof(tall));
        ASSERT(tall == fasit[i]);
    }
}

void test_klv_partial_move(void)
{
    // these tests fails if the padding is other than 2 bytes, TODO: make arrays for different padding sizes
    uint8_t partial[] = {
        0x01, 0x02, 0xAA, 0xAA, 0x00, 0x1B, 0x02, 0x02, 0xBB, 0xBB, 0x00, 0x36, 0xFF, 0xFF, 0xFF, 0xFF, // first block thats almost full
        0x01, 0x02, 0xAA, 0xAA, 0x00, 0x1B, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF // second block thats partially copied
    };
    flash_t* flash = ram_flash_init(partial, sizeof(partial), sizeof(partial) / 2, false);
    klv_t klv;
    klv_init(&klv, flash, 0, sizeof(partial) / 2, sizeof(partial) / 2);
    uint16_t u = 0;

    // verify that we can read the entries and that they are correct
    ASSERT(klv_read(&klv, 1, &u, sizeof(u)) == sizeof(u));
    ASSERT(u == 0xAAAA);
    ASSERT(klv_read(&klv, 2, &u, sizeof(u)) == sizeof(u));
    ASSERT(u == 0xBBBB);
}

void test_klv(void)
{
    test_klv_init();
    test_klv_read_write();
    test_klv_stress();
    test_klv_partial_move();
}