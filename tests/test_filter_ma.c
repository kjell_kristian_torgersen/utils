#include <math.h>
#include <stdint.h>

#include "../filter_ma.h"
#include "../filter_ma2.h"
#include "tests.h"

#define TEST_ELEMS 5

static void test_init16(void)
{
    int16_t storage[TEST_ELEMS];

    filter_ma16_t filter;
    filter_ma16_init(&filter, storage, TEST_ELEMS);
    ASSERT(filter.x == storage);
    ASSERT(filter.idx == 0);
    ASSERT(filter.n == TEST_ELEMS);
    ASSERT(filter.acc == 0);
    for (int i = 0; i < TEST_ELEMS; i++) {
        ASSERT(filter.x[i] == 0);
    }
}

static void test_process16(void)
{
    int16_t storage[TEST_ELEMS];
    // convolution between 1,1,1,1,1 and 1,1,1,1,1,1,1 (and some extra zeros added)
    const int16_t expected_output[] = { 1, 2, 3, 4, 5, 5, 5, 4, 3, 2, 1, 0, 0, 0, 0, 0 };

    filter_ma16_t filter;
    filter_ma16_init(&filter, storage, TEST_ELEMS);

    int16_t x = 1;
    int16_t y;

    for (int i = 0; i < (int)(sizeof(expected_output) / sizeof(expected_output[0])); i++) {
        if (i >= 7) {
            x = 0;
        }
        y = filter_ma16_process(&filter, x);
        ASSERT(y == expected_output[i]);
        if (y != expected_output[i]) {
            printf("y = %d, expected_output[%d] = %d\n", y, i, expected_output[i]);
        }
    }
}

static void test_init32(void)
{
    int32_t storage[TEST_ELEMS];

    filter_ma32_t filter;
    filter_ma32_init(&filter, storage, TEST_ELEMS);
    ASSERT(filter.x == storage);
    ASSERT(filter.idx == 0);
    ASSERT(filter.n == TEST_ELEMS);
    ASSERT(filter.acc == 0);
    for (int i = 0; i < TEST_ELEMS; i++) {
        ASSERT(filter.x[i] == 0);
    }
}

static void test_process32(void)
{
    int32_t storage[TEST_ELEMS];
    // convolution between 1,1,1,1,1 and 1,1,1,1,1,1,1 (and some extra zeros added)
    const int32_t expected_output[] = { 1, 2, 3, 4, 5, 5, 5, 4, 3, 2, 1, 0, 0, 0, 0, 0 };

    filter_ma32_t filter;
    filter_ma32_init(&filter, storage, TEST_ELEMS);

    int32_t x = 1;
    int32_t y;

    for (int i = 0; i < (int)(sizeof(expected_output) / sizeof(expected_output[0])); i++) {
        if (i >= 7) {
            x = 0;
        }
        y = filter_ma32_process(&filter, x);
        ASSERT(y == expected_output[i]);
        if (y != expected_output[i]) {
            printf("y = %d, expected_output[%d] = %d\n", y, i, expected_output[i]);
        }
    }
}

#define ORDER 4
#define STAGES 10

void test_filter_ma(void)
{
    test_init16();
    test_process16();
    test_init32();
    test_process32();
#if 0
    int32_t storage0[STAGES * ORDER];

    filter_ma32_t filter0;

    filter_ma2_t filter_ma2;

    int32_t storage4[STAGES * ORDER];
    int32_t acc[STAGES];

    filter_ma2_init(&filter_ma2, storage4, acc, ORDER, STAGES, 0);
    filter_ma32_init(&filter0, storage0, STAGES * ORDER);

    int32_t x = 1;

    int32_t y0;
    int32_t y1;

    for (int i = 0; i < 16 * 3 * ORDER; i++) {
        /*if ((i >> 4) & 1) {
            x = 0;
        } else {
            x = 1;
        }*/
        x = 10 * (i >> 5);

        y0 = filter_ma32_process(&filter0, x);
        y1 = filter_ma2_process(&filter_ma2, x);

        printf("%d %f %f\n", x, y0 / (double)(STAGES * ORDER), y1 / (double)(powf(ORDER, STAGES)));
    }
    for (int i = 0; i < STAGES * ORDER; i++) {
        printf("%d %d\n", i, storage4[i]);
    }
#endif
}