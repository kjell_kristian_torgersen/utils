#include <string.h>

#include "../klv.h"

#include "api/flash.h"

#include "tests/ram_flash.h"

#include "tests.h"

#define FLASH_CAPACITY 128
#define FLASH_PAGE_SIZE (FLASH_CAPACITY / 2)

extern flash_t* ram_flash_init(void* storage, int capacity, int page_size, bool erase);

bool check_flash_empty(flash_t* flash)
{
    for (uint32_t i = 0; i < flash->capacity; i++) {
        if (flash->data[i] != 0xFF) {
            return false;
        }
    }
    return true;
}

void display_flash(flash_t* flash)
{
    printf("flash:\n");
    for (uint32_t i = 0; i < flash->capacity; i++) {
        if (i == flash->capacity / 2) {
            printf("\n");
        }
        printf("%02X ", flash->data[i]);
    }
    printf("\n");
}

void test_ram_flash(void)
{
    char storage[FLASH_CAPACITY];
    flash_t* flash = ram_flash_init(storage, FLASH_CAPACITY, FLASH_PAGE_SIZE, true);
    uint8_t buf[5];
    ASSERT(flash != NULL);
    if (flash == NULL) {
        return;
    }
    ASSERT(flash->capacity == FLASH_CAPACITY);
    ASSERT(flash->page_size == FLASH_PAGE_SIZE);
    ASSERT(flash->data != NULL);

    ASSERT(check_flash_empty(flash));

    // //display_flash(flash);
    ASSERT(flash_write(flash, 0, "Hello", 5) == 5);
    // //display_flash(flash);

    ASSERT(flash_read(flash, 0, buf, 5) == 5);
    ASSERT(memcmp(buf, "Hello", 5) == 0);

    ASSERT(flash_erase(flash, 0, FLASH_PAGE_SIZE - 1) == 0);
    ASSERT(!check_flash_empty(flash));

    ASSERT(flash_erase(flash, 0, FLASH_PAGE_SIZE) == FLASH_PAGE_SIZE);
    // //display_flash(flash);
    ASSERT(check_flash_empty(flash));

    ASSERT(flash_erase(flash, 0, 2 * FLASH_PAGE_SIZE - 1) == FLASH_PAGE_SIZE);
    ASSERT(flash_erase(flash, 0, 2 * FLASH_PAGE_SIZE) == 2 * FLASH_PAGE_SIZE);
    ASSERT(flash_write(flash, 0, "\x0F", 1) == 1);
    // //display_flash(flash);
    ASSERT(flash_write(flash, 0, "\xF0", 1) == 1);
    //   //display_flash(flash);
    uint8_t c = 0xFF;
    ASSERT(flash_read(flash, 0, &c, 1) == 1);
    ASSERT(c == 0x00);
    ASSERT(flash_close(NULL) == RET_NULL_POINTER);
    ASSERT(flash_close(flash) == RET_OK);
}