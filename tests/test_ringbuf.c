#include "tests.h"

#include "../ringbuf.h"

void test_ringbuf(void)
{
    uint8_t buf[16];
    ringbuf_t rb;
    uint8_t tmp[4] = { 1, 2, 3, 4 };
    uint8_t tmp2[4] = { 5, 6, 7, 8 };
    uint8_t big[32] = { 0 };
    ringbuf_init(&rb, buf, sizeof(buf));
    ASSERT(rb.storage == buf);
    ASSERT(rb.read == 0);
    ASSERT(rb.write == 0);
    ASSERT(rb.storage_size == sizeof(buf));

    ASSERT(ringbuf_len(&rb) == 0);
    ASSERT(ringbuf_free_space(&rb) == (sizeof(buf) - 1));
    ASSERT(ringbuf_read(&rb, tmp2, sizeof(tmp2)) == 0);
    ASSERT(tmp2[0] == 5);
    ASSERT(ringbuf_len(&rb) == 0);
    ASSERT(ringbuf_free_space(&rb) == (sizeof(buf) - 1));
    ASSERT(ringbuf_write(&rb, tmp, sizeof(tmp)) == sizeof(tmp));
    ASSERT(ringbuf_len(&rb) == sizeof(tmp));
    ASSERT(ringbuf_free_space(&rb) == ((sizeof(buf) - 1) - sizeof(tmp)));
    ASSERT(ringbuf_read(&rb, tmp2, sizeof(tmp2)) == sizeof(tmp2));
    ASSERT(array_cmp(tmp, tmp2, sizeof(tmp)));
    ASSERT(ringbuf_free_space(&rb) == (sizeof(buf) - 1));
    ASSERT(ringbuf_len(&rb) == 0);
    ASSERT(ringbuf_write(&rb, big, sizeof(big)) == (sizeof(buf) - 1));
    ASSERT(ringbuf_read(&rb, big, sizeof(big)) == (sizeof(buf) - 1));
}