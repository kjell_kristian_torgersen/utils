
#include <string.h>

#include "tests.h"

#include "../comp.h"
#include "../decomp.h"
#include "../src/comp_priv.h"

static uint8_t flash_buf[64] = { 0x0 };
static uint8_t flash_buf_idx = 0;

static int test_write(void* arg, const void* data, int count)
{
    (void)arg;
    memcpy(flash_buf + flash_buf_idx, data, count);
    // printf("%02X ", flash_buf[flash_buf_idx]);
    /*for (int i = 0; i < 8; i++) {
        if (i == 4) {
            printf(" ");
        }
        printf("%i", !!(flash_buf[flash_buf_idx] >> i));
    }*/
    // printf("\n");
    flash_buf_idx += count;
    return count;
}

static int test_read(void* arg, void* data, int count)
{
    (void)arg;
    memcpy(data, flash_buf + flash_buf_idx, count);
    flash_buf_idx += count;
    return count;
}

static void test_comp_init(void)
{
    int arg;
    comp_t comp;
    comp_init(&comp, test_write, &arg);
    ASSERT(comp.writer == test_write);
    ASSERT(comp.writer_arg == &arg);
    ASSERT(comp.byte == 0);
    ASSERT(comp.idx == 0);
    ASSERT(comp.last_value == 0);
    ASSERT(comp.repeat == 0);
}

static void test_decomp_init(void)
{
    int arg;
    decomp_t decomp = { 0 };
    decomp_init(&decomp, test_read, &arg);
    ASSERT(decomp.reader == test_read);
    ASSERT(decomp.reader_arg == &arg);
    ASSERT(decomp.byte == 0);
    ASSERT(decomp.idx == 2);
    ASSERT(decomp.value == 0);
    ASSERT(decomp.last_value == 0);
    ASSERT(decomp.repeat == 0);
}

static void test_comp_write(void)
{
    uint16_t fasit[] = { 1, 2, 2, 2, 2, 3, 4, 8 };
    comp_t comp;
    decomp_t decomp;

    comp_init(&comp, test_write, NULL);
    flash_buf_idx = 0;
    decomp_init(&decomp, test_read, NULL);
    memset(flash_buf, 0xFF, sizeof(flash_buf));
    for (unsigned i = 0; i < sizeof(fasit) / sizeof(fasit[0]); i++) {
        comp_write(&comp, fasit[i]);
    }
    for (int i = 0; i < 1001; i++) {
        comp_write(&comp, 9);
    }
    comp_finalize(&comp);
    printf("\n");
    ASSERT(((flash_buf[0] >> 4) & COMP_DIFF_MASK) == COMP_DIFF);
    ASSERT((flash_buf[0] >> 4 & ~COMP_DIFF_MASK) == (1 + 4));
    // ASSERT(flash_buf_idx == 22);
    flash_buf_idx = 0;
    for (unsigned i = 0; i < sizeof(fasit) / sizeof(fasit[0]); i++) {
        uint16_t value;
        if (decomp_read(&decomp, &value)) {
            if (value != fasit[i]) {
                printf("%i. %i != %i\n", i, value, fasit[i]);
                ASSERT(value == fasit[i]);
            }
        }
    }
    for (int i = 0; i < 1001; i++) {
        uint16_t value;
        if (decomp_read(&decomp, &value)) {
            if (value != 9) {
                printf("%i. %i != %i\n", i, value, 9);
                ASSERT(value == 9);
            }
        }
    }
    uint16_t value;
    ASSERT(decomp_read(&decomp, &value) == false);
}

static const uint8_t decomp_data[] = {
    0x81, 0x6e, 0xcf, 0x78, 0x17, 0x68, 0x17, 0xb8, 0x18, 0x08, 0x18, 0x58, 0x18, 0x97, 0x76, 0x76, 0x6c, 0x17, 0xc1, 0x66, 0xc1, 0x6c, 0x16, 0xc2, 0x6c, 0x26, 0xc2, 0x6c, 0x36, 0xca, 0x2c, 0x72, 0xc7, 0x2c, 0x92, 0xc8, 0x2c, 0xa2, 0xca, 0x2c, 0xa2, 0xcc, 0x2c, 0xb2, 0xcd, 0x2c, 0xf2, 0xce, 0x2c, 0xf2, 0xd0, 0x2d, 0x02, 0xce, 0x2d, 0x42, 0xda, 0x2d, 0x42, 0xda, 0x2d, 0xe2, 0xdc, 0x2d, 0x72, 0xe7, 0x2e, 0xa2, 0xed, 0x2f, 0x92, 0xfe, 0x2f, 0xee, 0xb2, 0xfe, 0xfe, 0xfe, 0xfe, 0xc5, 0x2f, 0xef, 0xef, 0xef, 0xee, 0x56, 0xfe, 0xfe, 0xe5, 0x6f, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xee, 0x46, 0xfe, 0xfe, 0xfe, 0xd3, 0x6f, 0xef, 0xc2, 0xfe, 0xfe, 0xfe, 0xda, 0x2f, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xed, 0xc6, 0xfe, 0xfe, 0xfe, 0xf0, 0x2f, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0x06, 0xfe, 0xd6, 0x6f, 0xed, 0x66, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xc3, 0x2f, 0xef, 0xee, 0x62, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xee, 0x2f, 0xef, 0xef, 0x22, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xe5, 0x2f, 0xef, 0xef, 0xef, 0xef, 0xef, 0xed, 0x02, 0xfe, 0xfe, 0xfe, 0xfe, 0xe3, 0x2f, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0x02, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xf3, 0x6f, 0xef, 0xef, 0xef, 0xef, 0xef, 0xec, 0x42, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xf3, 0x2f, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xed, 0xd2, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xe3, 0x2f, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xef, 0xed, 0x62, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xff, 0xff
};

static uint16_t decomp_data_idx = 0;

int test_decomp_read(void* arg, void* buf, int size)
{
    (void)arg;
    memcpy(buf, decomp_data + decomp_data_idx, size);
    decomp_data_idx += size;
    return size;
}

void test_decomp(void)
{

    decomp_t decomp;
    decomp_init(&decomp, test_decomp_read, NULL);
    uint16_t value;
    int counter = 1000000;
    int i = 0;
    // printf("Time [s]\tTemperature [C]\n");
    while (decomp_read(&decomp, &value) && counter--) {
        // printf("%i\t%f\n", i, value / 8.0 - 20.0);
        i++;
    }
}

void test_comp(void)
{
    test_comp_init();
    test_decomp_init();
    test_comp_write();
    test_decomp();
    // uint16_t last = 0;
    // uint16_t test[] = { 1, 2, 3, 2, 3, 2, 2, 1, 2, 1, 2, 3, 2, 3, 2, 4, 5, 6, 7, 8, 9 };
    // for (unsigned i = 0; i < sizeof(test) / sizeof(test[0]); i++) {
    //     uint16_t value = comp_hyst(&last, test[i]);
    //     printf("%i %i\n", test[i], value);
    // }
}