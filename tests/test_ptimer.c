#include <stddef.h>

#include "tests.h"

#include "../ptimer.h"

static int timer1_check = 0;
static int timer2_check = 0;
static int timer3_check = 0;

static void timer1_callback(void* arg)
{
    ASSERT(arg == (void*)1 || arg == (void*)3);
    timer1_check++;
}

static void timer2_callback(void* arg)
{
    ASSERT(arg == (void*)2);
    timer2_check++;
}

static void timer3_callback(void* arg)
{
    ASSERT(arg == NULL);
    timer3_check++;
}

void test_ptimer(void)
{
    ptimer_t timer[3] = { 0 };
    ASSERT(timer[0].arg == NULL);
    ASSERT(timer[0].callback == NULL);
    ASSERT(timer[0].next == NULL);
    ASSERT(timer[0].period == 0);
    ASSERT(timer[0].start == 0);

    ptimer_configure(&timer[0], 100, timer1_callback, (void*)1);

    ASSERT(timer[0].arg == (void*)1);
    ASSERT(timer[0].callback == timer1_callback);
    ASSERT(timer[0].next == NULL);
    ASSERT(timer[0].period == 100);
    ASSERT(timer[0].start == 0);

    ptimer_configure(&timer[1], 200, timer2_callback, (void*)2);
    ptimer_configure(&timer[2], 300, timer3_callback, NULL);

    ASSERT(timer1_check == 0);
    ASSERT(timer2_check == 0);
    ASSERT(timer3_check == 0);
    ptimer_update(99);
    ASSERT(timer1_check == 0);
    ASSERT(timer2_check == 0);
    ASSERT(timer3_check == 0);
    ptimer_update(100);
    ASSERT(timer1_check == 1);
    ASSERT(timer2_check == 0);
    ASSERT(timer3_check == 0);

    ptimer_update(199);
    ASSERT(timer1_check == 1);
    ASSERT(timer2_check == 0);
    ASSERT(timer3_check == 0);
    ptimer_update(200);
    ASSERT(timer1_check == 2);
    ASSERT(timer2_check == 1);
    ASSERT(timer3_check == 0);

    ptimer_update(299);
    ASSERT(timer1_check == 2);
    ASSERT(timer2_check == 1);
    ASSERT(timer3_check == 0);
    ptimer_update(300);
    ASSERT(timer1_check == 3);
    ASSERT(timer2_check == 1);
    ASSERT(timer3_check == 1);

    ptimer_configure(&timer[0], 400, timer1_callback, (void*)3);

    ASSERT(timer[0].arg == (void*)3);
    ASSERT(timer[0].callback == timer1_callback);
    ASSERT(timer[0].next == NULL);
    ASSERT(timer[0].period == 400);
    ASSERT(timer[0].start == 0);

    ptimer_update(399);
    ASSERT(timer1_check == 3);
    ASSERT(timer2_check == 1);
    ASSERT(timer3_check == 1);

    ptimer_update(400);
    ASSERT(timer1_check == 4);
    ASSERT(timer2_check == 2);
    ASSERT(timer3_check == 1);

    ASSERT(timer[0].start == 400);
}
