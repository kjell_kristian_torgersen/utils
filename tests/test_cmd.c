#include <stddef.h>
#include <string.h>

#include "tests.h"

#include "../cmd.h"

cmd_response_t cmd_test(cmd_ctx_t* ctx, const char* args)
{
    if (args == NULL) {
        return CMD_RESPONSE_UNKNOWN;
    }
    ctx->printf("hello from test\n");

    if (!strcmp(args, "argstest")) {
        return CMD_RESPONSE_OK;
    } else {
        return CMD_RESPONSE_INVALID_ARGUMENT;
    }
}

static bool test_cmd_pass = false;
int test_cmd_helper(const char* __restrict __fmt, ...)
{
    if (!strcmp(__fmt, "hello from test\n")) {
        test_cmd_pass = true;
    }
    return 0;
}

void test_cmd(void)
{
    cmd_ctx_t cmd_ctx = { test_cmd_helper };
    cmd_t cmd = { "test", "test", &cmd_test };
    ASSERT(cmd_parse("test blabla", &cmd_ctx, &cmd, 1) == CMD_RESPONSE_INVALID_ARGUMENT);
    ASSERT(cmd_parse("test argstest", &cmd_ctx, &cmd, 1) == CMD_RESPONSE_OK);
    ASSERT(cmd_parse("test", &cmd_ctx, &cmd, 1) == CMD_RESPONSE_UNKNOWN);
    ASSERT(test_cmd_pass == true);
}