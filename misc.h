/*
 * misc.h
 *
 *  Created on: Oct 16, 2020
 *      Author: kjell
 */

#ifndef UTILS_MISC_H_
#define UTILS_MISC_H_

#ifndef MIN
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#endif

void misc_showArr(const void* buf, int count);
void misc_print_float(float f);

#endif /* UTILS_MISC_H_ */
