#ifndef F7C88D8B_D992_4B81_815B_9FE4AE7E6920
#define F7C88D8B_D992_4B81_815B_9FE4AE7E6920

#include <stdbool.h>
#include <stdint.h>

typedef struct modbus_parser {
    uint8_t* buf;
    int buf_idx;
    int buf_size;
} modbus_parser_t;

bool modbus_frame_parse_init(modbus_parser_t* parser, void* storage, int storage_size);
bool modbus_frame_parse_reset(modbus_parser_t* parser);
bool modbus_frame_parse_process(modbus_parser_t* parser, char c);

int modbus_write_holding_registers_request(void* buf, int buf_size, int slave_id, int register_address, const uint16_t* registers, int register_count);
/**
 * @brief modbus_read_holding_registers
 * @param buf MODBUS data package to parse
 * @param buf_size size of MODBUS data package in bytes
 * @param slave_id Address of slave, 0-247
 * @param register_address Address of first register to read 0-9999
 * @param register_count Number of register to read 1-125
 * @param registers Pointer to array of registers to read
 * @return Frame size in number of bytes
 */
int modbus_read_holding_registers_request(void* buf, int buf_size, int slave_id, int register_address, int register_count);

int modbus_read_holding_registers_response(const void* buf, int buf_size, const uint16_t* registers);

#endif /* F7C88D8B_D992_4B81_815B_9FE4AE7E6920 */
