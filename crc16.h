#ifndef CFA6FC14_E604_4AC6_AC3B_89B00D330BA2
#define CFA6FC14_E604_4AC6_AC3B_89B00D330BA2

#include <stdbool.h>
#include <stdint.h>

uint16_t crc16_reverse(uint16_t c);
uint16_t crc16_update(uint16_t crc, uint16_t poly, uint8_t a);
void	 crc16_fill_table(uint16_t *table, uint16_t poly, bool refin, bool refout);
uint16_t crc16_use_table(const uint16_t *table, uint16_t crc, const void *buf, int count);
uint16_t crc16_use_reftable(const uint16_t *table, uint16_t crc, const void *buf, int count);
uint16_t crc16_process(uint16_t poly, uint16_t crc, bool refin, const void *buf, int count);

#endif /* CFA6FC14_E604_4AC6_AC3B_89B00D330BA2 */
