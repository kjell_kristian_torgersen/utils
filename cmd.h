#ifndef C3A730C0_06B6_43F4_BA43_25E7936F361B
#define C3A730C0_06B6_43F4_BA43_25E7936F361B

#include <stdint.h>

typedef struct cmd_ctx {
    int (*printf)(const char* fmt, ...);
} cmd_ctx_t;

typedef enum cmd_response {
    CMD_RESPONSE_OK,
    CMD_RESPONSE_ERROR,
    CMD_RESPONSE_INVALID_ARGUMENT,
    CMD_RESPONSE_INVALID_COMMAND,
    CMD_RESPONSE_INVALID_STATE,
    CMD_RESPONSE_INVALID_VALUE,
    CMD_RESPONSE_NOT_IMPLEMENTED,
    CMD_RESPONSE_NOT_SUPPORTED,
    CMD_RESPONSE_UNKNOWN
} cmd_response_t;

typedef cmd_response_t (*cmd_callback_t)(cmd_ctx_t* ctx, const char* args);

typedef struct cmd {
    char* cmd; ///< The command itself
    char* help; ///< The help text
    cmd_callback_t callback; ///< The callback function
} cmd_t;

uint64_t cmd_uint64(const char* __restrict__ __n, char** __restrict__ __end_PTR);

/** \brief Parse a command line and execute the command.
 * \param *cmd The command line to parse.
 * \param *ctx The command context to use.
 * \param *cmds The list of available commands.
 * \param ncmds The number of commands in the list.
 * \return The response code.
 * \see cmd_response_t
 * \see cmd_t
 * \see cmd_callback_t
 */
cmd_response_t cmd_parse(const char* cmd, cmd_ctx_t* ctx, const cmd_t* cmds, int ncmds);
const char* cmd_response_str(cmd_response_t response);

#endif /* C3A730C0_06B6_43F4_BA43_25E7936F361B */
