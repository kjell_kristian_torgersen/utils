#ifndef C93EFAAD_4A59_47E8_BFDE_295F17004BE5
#define C93EFAAD_4A59_47E8_BFDE_295F17004BE5

#include <stdbool.h>

#include "linbuf.h"

bool input_parseChar(linbuf_t* lb, char c);
const char* input_str(linbuf_t* lb);
void input_clear(linbuf_t* lb);

#endif /* C93EFAAD_4A59_47E8_BFDE_295F17004BE5 */
