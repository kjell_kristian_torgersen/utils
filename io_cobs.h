#ifndef F6773BA9_D252_4D75_A8D5_96ED46590CBB
#define F6773BA9_D252_4D75_A8D5_96ED46590CBB

#include "api/io.h"

io_t* io_cobs_init(io_t* next, void* rxbuf, int rxbufsize);

#endif /* F6773BA9_D252_4D75_A8D5_96ED46590CBB */
