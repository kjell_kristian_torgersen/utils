#ifndef DD55977B_6CE4_4569_BD3A_CC9EF86EBEE1
#define DD55977B_6CE4_4569_BD3A_CC9EF86EBEE1

#include <stdint.h>

#include "../cmd.h"

typedef enum parameter_type {
    PARAMETER_TYPE_INT,
    PARAMETER_TYPE_UINT,
    PARAMETER_TYPE_FLOAT,
    PARAMETER_TYPE_STRING,
    PARAMETER_TYPE_BYTES,
    PARAMETER_TYPE_BOOL,
} parameter_type_t;

typedef struct parameter {
    const char* name; // name of the parameter
    void* value; // value of the parameter
    parameter_type_t type; // type of the parameter
    uint8_t size; // in bytes
    void (*set)(struct parameter* param, void* arg); // setter function (write to flash, NULL for no write)
    void* arg; // argument for the setter function
} parameter_t;

cmd_response_t parameter_parse(const char* param, const parameter_t* params, int params_count);

#endif /* DD55977B_6CE4_4569_BD3A_CC9EF86EBEE1 */
