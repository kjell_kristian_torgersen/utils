#ifndef BB143E79_C734_4DEA_971D_862043AEA923
#define BB143E79_C734_4DEA_971D_862043AEA923

#include <stdint.h>

typedef struct filter_ma2 {
    int32_t* x;
    int32_t* acc;
    int idx;
    int order;
    int stages;
    int shift;
} filter_ma2_t;

void filter_ma2_init(filter_ma2_t* this, int32_t* storage, int32_t* acc, int order, int stages, int shift);
int32_t filter_ma2_process(filter_ma2_t* this, int32_t x);

#endif /* BB143E79_C734_4DEA_971D_862043AEA923 */
