# Utility functions in C mainly for microcontrollers
Typically this project is added as a submodule to a main project, and the required files from this are referenced in the main project.

It does however contain some unit tests so can be built and executed for verification. In order to do this, a submodule called api must be present in the tests folder.

## Components
| Name | Description |
| ---- | ----------- |
| aes128 | AES128 encryption/decryption implementation. |
| chacha20 | Chacha20 encryption/decryption implementation. |
| cmd | Command line interface, to parse and execute text commands. |
| cobs_encoder | For transmitting cobs messages with callback write function. |
| cobs | COBS encoder and decoder functions for working with arrays. |
| comp/decomp | Compression algorithm that is extremely efficient for slowly and little changing values. |
| crc8 | CRC8 implementation. Supports both using and creating tables, or not use tables at all. |
| crc16 | CRC16 implementation. Supports both using and creating tables, or not use tables at all. |
| crc32 | CRC32 implementation. |
| crc64 | CRC64 implementation. |
| datareceived_event | Attempt to implement publish subscribe pattern in embedded C. |
| filter_ma | Moving average filter that should be fairly efficient and only use integers. |
| filter_ma2| Multiple cascaded moving average filters. |
| iir.h | IIR filter implementation with floats. |
| input | Handle typical keyboard input from UART/serial port or similar. |
| io_cobs | Use COBS with the IO interface. |
| klv | Key, Value and Length implementation. For storing key/value pairs in flash, that can be read/written randomly. |
| linbuf | Just a linear buffer. |
| minmax | Keep track of min, max and sum of uint16 values. Typically for use with ADC measurements. |
| misc | Convenience functions to print arrays and floats without support for floats. |
| modbus | MODBUS master not implementated yet. |
| mutex | A software mutex. |
| parameter | Support for setting parameters with command line. |
| ptimer | Periodic timers based on callback functions. |
| random | Pseudo-random number generator |
| ringbuf | Ring buffer |
