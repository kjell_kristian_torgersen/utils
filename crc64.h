#ifndef DD17D2F4_CAC5_4C39_A310_1CE73D919FC8
#define DD17D2F4_CAC5_4C39_A310_1CE73D919FC8
#include <stdint.h>

uint64_t crc64_update(uint64_t crc, uint64_t poly, uint8_t a);
uint64_t crc64_process(uint64_t crc, uint64_t poly, const void* buf, int count);

#endif /* DD17D2F4_CAC5_4C39_A310_1CE73D919FC8 */
