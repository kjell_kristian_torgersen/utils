#ifndef BC37FF2A_5116_4FA5_AB0C_708F1DA1A80E
#define BC37FF2A_5116_4FA5_AB0C_708F1DA1A80E

#include <stdint.h>

uint32_t crc32_reverse(uint32_t c);
uint32_t crc32_update(uint32_t crc, uint32_t poly, uint8_t a);
uint32_t crc32_process(uint32_t poly, uint32_t crc, bool refin, const void* buf, int count);

#endif /* BC37FF2A_5116_4FA5_AB0C_708F1DA1A80E */
