#ifndef E6D3FAAC_D260_4CFC_852B_0BC6E91B825A
#define E6D3FAAC_D260_4CFC_852B_0BC6E91B825A

#include <stdint.h>

int cobs_encode(void* dst, int dstsize, const void* src, int srcsize);
int cobs_decode(void* dst, int dstsize, const void* src, int srcsize);

#endif /* E6D3FAAC_D260_4CFC_852B_0BC6E91B825A */
