#include <stdio.h>

#include "decomp.h"

int file_reader(void* arg, void* buf, int size)
{
    return fread(buf, 1, size, arg);
}

int main(int argc, char const* argv[])
{
    decomp_t decomp;
    FILE* fin = fopen(argv[1], "rb");
    FILE* fout = fopen(argv[2], "wb");

    decomp_init(&decomp, file_reader, fin);
    uint16_t value;
    int i = 0;
    fprintf(fout, "Time [s]\tTemperature [C]\n");
    while (decomp_read(&decomp, &value)) {
        fprintf(fout, "%i\t%f\n", i, value / 8.0 - 20.0);
        i++;
    }
    fclose(fin);
    fclose(fout);
    return 0;
}
